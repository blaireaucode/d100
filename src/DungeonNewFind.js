/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import DiceTestHelp from './DiceTestHelp.js';
import DiceTest from './DiceTest.js';
import update from 'immutability-helper';
import * as dh from './d100_helpers.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class DungeonNewFind extends Component {

    constructor(props) {
        super(props);
        this.onNewFindTestCompleted = this.onNewFindTestCompleted.bind(this);
        this.onNewFindTestStarted = this.onNewFindTestStarted.bind(this);
    }

    onNewFindTestStarted() {
        const mod = dh.get_area_find_modifier(this.props.area);
        const test = {
            id:'new_find',
            state:'init',
            dices:[{dice_max:100, mod:mod}],
        };
        return test;
    }

    onNewFindTestCompleted(test) {
        // get a door
        var f = dh.get_find(test);

        // add to area
        const a = update(this.props.area, {findings: {$push:[f]}});
        this.props.set('area', a);

        // status
        const s = 'You find n°'+f.dice_value+' '+f.details;
        const l = update(this.props.log, {$push:[s]});
        this.props.set('log', l);
    }

    render() {
        var t = '';
        if (this.props.children) t = this.props.children;
        else t = 'Roll to search the area';
        const mod = dh.get_area_find_modifier(this.props.area);
        return (
            <span>
              <DiceTestHelp id={'new_find'}>                
                Roll D100 (+{mod}) to find something (hopefully)
                interesting from Table F.                
                <br/>
                Only once by area.
                <br/>
                Modifier: yellow 0, red +10, green +5, blue +20.
              </DiceTestHelp>
              {' '}
              <DiceTest
                id={'new_find'}
                onRollingStarted={this.onNewFindTestStarted}
                onRollingCompleted={this.onNewFindTestCompleted}
              > {' '}
                {t}</DiceTest>
            </span>
        );

    }

}

export default connect(mapStateToProps, mapDispatchToProps)(DungeonNewFind);

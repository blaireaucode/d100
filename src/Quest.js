/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';
import * as dh from './d100_helpers';

class Quest extends Component {

    onClickStart() {
        const d = dh.get_new_dungeon();
        this.props.dungeon_set(d);
    }
    
    onClickRemove() {
        this.props.dungeon_set({});
    }
    
    render() {
        console.log('path', this.props.location.pathname);
        console.log('path', this.props);
        
        return (
            <div>              
              Quest.

              <p/>

              <Link to={'/dungeon/'}
                    onClick={this.onClickStart.bind(this)}
              >Start new dungeon</Link>

              <p/>
              
              <Link to={'/dungeon/'}
                    onClick={this.onClickRemove.bind(this)}
              >Remove dungeon</Link>
              
              
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Quest);

/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import table_ea from './db/table_ea.js';

/*
  FIXME encounter_get_typename(encounter)
  FIXME encounter_get_abilities(encounter)
  abilities_to_string(abilities)
  string_to_abilities(str)
*/


// ----------------------------------------------------------------------------
export function encounter_get_typename(encounter)
{
    var type='Humanoid';
    switch (encounter.Type) {
    case 'c': type = 'Creature'; break;
    case 'd': type = 'Demon'; break;
    case 'ud': type = 'Undead'; break;
    default: type='Humanoid'; 
    }
    return type;
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function encounter_get_abilities(encounter)
{
    var abilities = [];
    const a = encounter.Abilities.trim();
    var words = a.split(",");
    console.log('abilities:',words);
    console.log('table_ea:',table_ea);
    
    for (var v of words) {
        console.log('v',v);
        abilities.push({name:v, help:table_ea[v]});
        console.log('table',table_ea[v]);
    }
    return abilities;
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function abilities_to_string(abilities)
{
    var str = '';
    if (!abilities) return '';
    for(var a of abilities) {
        str += a.name+', ';
    }
    str = str.substring(0, str.length - 2);
    return str;
}
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
export function string_to_abilities(str)
{
    var abilities = [];
    const words = str.split(',');
    for(var w of words) {
        w = w.trim();
        //console.log('w', w);
        // if (w in table_ea)
        // if (w.length>1)
        abilities.push({name:w});
    }
    //    console.log('ab', abilities);
    return abilities;
}
// ----------------------------------------------------------------------------

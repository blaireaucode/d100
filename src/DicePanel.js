/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Dices from './Dices.js';
import DiceFinalLink from './DiceFinalLink.js';
import * as dh from './d100_helpers.js';
import update from 'immutability-helper';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class DicePanel extends Component {

    constructor(props) {
        super(props);
        this.onClickEnd = this.onClickEnd.bind(this);
        this.onDiceUnmount = this.onDiceUnmount.bind(this);
    }
    
    onClickEnd(event) {
        const id = this.props.tests['current_id'];
        const test = this.props.tests[id];
        const t = dh.test_end(test);      
        this.props.test_set(id, t);
        if (this.props.onRollingCompleted) this.props.onRollingCompleted();
    }
    
    onDiceUnmount() {
        const id = this.props.tests['current_id'];
        const test = this.props.tests[id];
        if (test.state === 'rolling') {
            const t = update(test, {state:{$set:'anim_completed'}});
            this.props.test_set(id, t);
            this.onClickEnd();
        }
    }

    
    render() {
        // get the id of the current test
        const id = this.props.tests['current_id'];
        if (!id) return '';
        const test = this.props.tests[id];
        if (!test) return '';
        const dices = test.displayed_dices;        
        return (
            <div>
              <DiceFinalLink 
                delay={2500} // FIXME put delay as parameters in store
                test={test}
                onClickEnd={this.onClickEnd.bind(this)}/>
              <div>
                <Dices
                  height='22vh' // like in dice-panel CSS
                  posY={7} /* hauteur ?  */
                  onUnmount={this.onDiceUnmount}
                  dices={dices}/>
              </div>
            </div>
        );
    }

    
}

export default connect(mapStateToProps, mapDispatchToProps)(DicePanel);

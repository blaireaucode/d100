/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import * as at from './action-types.js';

export const adventurer_set =
    adventurer => ({ type:at.ADVENTURER_SET, adventurer:adventurer });

export const set =
    (element_name, value) => ({ type:at.SET,
                                element_name:element_name, value:value });

export const set_items =
    items => ({ type:at.SET_ITEMS, items:items });

export const encounter_set =
    encounter => ({ type:at.ENCOUNTER_SET, encounter:encounter });

export const dungeon_set =
    dungeon => ({ type:at.DUNGEON_SET, dungeon:dungeon });

export const test_set =
    (id,test) => ({ type:at.TEST_SET, id:id, test:test });



/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import HelpPopover from './HelpPopover.js';
import Grid from '@material-ui/core/Grid';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';
import tables from './db/tables.js';

class Table extends Component {

    constructor(props) {
        super(props);
        const t = tables[this.props.t];
        if (!t) {
            this.header='ERROR: no table '+this.props.t+' ?';
            this.rows='';
            return;
        }
        this.header = (<Grid item xs={12} className='table-header'>
                         {t.header}
                       </Grid>);
        this.rows = [];
        this.title = this.props.l || t.title;
        this.width = t.width? t.width+'ex':'30vh';
        var i=0;
        // console.log('r', t.rows);
        for (var row of t.rows) {
            // console.log('r', row);
            if (typeof(row) === 'string')
                this.rows.push(<Grid key={i} item xs={12}>
                                 {row}
                               </Grid>);
            else {
                for(var col in row) {
                    this.rows.push(
                        <Grid key={i} item>
                          {row[col].toString()}
                        </Grid>
                    );
                    i = i+1;
                }
            }
            i = i+1;
        }
    }

    render() {        
        return (
            <HelpPopover
              width={this.width}
              l={this.title}>
              <Grid
                container
                direction="row"
                justify="flex-start"
                alignItems="flex-start">
                {this.header}
                {this.rows}
              </Grid>
            </HelpPopover>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Table);

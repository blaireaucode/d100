/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React from 'react';
import update from 'immutability-helper';
import { DICE_TYPES } from './Dices.js';
import DiceTestHelp from './DiceTestHelp.js';
import Grid from '@material-ui/core/Grid';
import Field4 from './Field4.js';
import table_d from './db/table_d.js';
import table_f from './db/table_f.js';
import table_g from './db/table_g.js';
import table_w from './db/table_w.js';
import * as charac from './characteristics.js';

const uuidv1 = require('uuid/v1');
const table_m = require('./db/table_m.json');

// ----------------------------------------------------------------------------
/*
  Functions

  test = test_roll(test)
  test = test_end(test)

  area = get_area(test)
  door = get_door(test)
  find = get_find(test)
  doors = rm_door(doors, door)
  find = rm_find(finds, find)

  door = update_door(doors, doorid, door)
  mod = get_area_find_modifier(area)
  element = get_element_in_table(table, test)

  h = get_field_help(props)

*/
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function get_new_dungeon()
{
    const d = {
        name: 'no_name',
        quest: {},
        turns: [],
        time: 0,
        area: {},
        history: []
    };
    return d;
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function build_map_img()
{
    var img = {};
    img = {};
    var image  = new Image();
    const a = require('./images/void.png');
    image.src = a;
    img[0] = image;
    for(let i=1; i<101; i++) {
        const a = require('./images/'+i+'.png');
        image = new Image();
        image.src = a;
        img[i] = image;
    }
    console.log('Reading all 100 map images: DONE.');
    return img;
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function get_area(test)
{
    // Copy area from table
    const dice_value = test.dices[0].value;
    const index = table_m.map(function(e)
                              { return e.id; }).indexOf(dice_value);
    var area = JSON.parse(JSON.stringify(table_m[index]));

    // FIXME --> rotation here (from parameter)

    // Create doors array
    if (!area.doors_direction) area.doors_direction = [];
    area.doors = [];
    area.findings = [];
    area.geographics = [];

    // Build exits & init door
    var i=0;
    for(var e of area.exits) {
        var ex = { direction:e, is_door:false };
        // if (area.doors) {
        var found = area.doors_direction.indexOf(e);
        if (found !== -1) {
            area.doors.push({ id:i, direction:e });
            ex.is_door=true;
            ex.door_id = i;
            //}
        }
        area.exits[i] = ex;
        i = i+1;
    }

    // Determine if new combat
    // FIXME --> DEBUG !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //     if (area.color === 'red') {
    area.combat = { step:'step_start' };
    //  }

    return area;
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function test_roll(test)
{
    if (!test || !test.id) {
        console.log('ERROR test_roll, test does not exist', test);
        return test;
    }

    var t = JSON.parse(JSON.stringify(test));
    t.state = 'rolling';

    for(var d of t.dices) {
        const dice_max = d.dice_max;
        d.value = Math.floor(Math.random() * dice_max); // 00-max
    }

    // compute dices info
    t.displayed_dices=[];
    for(d of t.dices) {
        // D10
        if (d.dice_max === 10) {
            t.displayed_dices.push(
                {
                    type: DICE_TYPES.D10,
                    backColor: "black",
                    fontColor: "white",
                    value: d.value+1
                }
            );
            if (d.value === 0) d.value = 10;
            t.value = d.value;
        }
        // D6
        if (d.dice_max === 6) {
            d.value += 1;
            t.displayed_dices.push(
                {
                    type: DICE_TYPES.D6,
                    backColor: "black",
                    fontColor: "blue",
                    value: d.value
                }
            );
            t.value = d.value;
        }
        // D100
        if (d.dice_max === 100) {
            var d1 = Math.floor(d.value/10);
            var d2 = d.value-d1*10;
            if (d1 === 0 && d2 === 0) d.value = d.dice_max;
            //console.log('dices:', d1, d2);
            t.displayed_dices.push(
                {
                    type: DICE_TYPES.D10,
                    backColor: "black",
                    fontColor: "white",
                    value: d2+1
                }
            );
            t.displayed_dices.push(
                {
                    type: DICE_TYPES.D100,
                    backColor: "black",
                    fontColor: "green",
                    value: d1+1
                }
            );
            t.value = d.value;
        }
    };
    return t;
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function test_end(test)
{
    if (!test) {
        console.log('ERROR test_end, test does not exist', test);
        return test;
    }
    const t = update(test, {state:{$set:'roll_completed'}});
    return t;
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function get_encounter_reaction(test)
{
    /*
      const dice_value = test.dices[0].value;

      // parse dice value
      var l = '';
      for(var line of table_er) {
      const v = parse_dice_value(line);
      if (dice_value>=v.a && dice_value<=v.b) {
      l = v.line;
      continue;
      }
      }

      const reaction = { dice_value:dice_value, txt:l.trim()};
      // console.log('reaction', reaction);
      return reaction;
    */
}
// ----------------------------------------------------------------------------



// ----------------------------------------------------------------------------
export function get_enc_attack(enc, test)
{
    const v = test.dices[0].value;

    // FIXME add bonus etc
    const success = (v <= enc.AV);
    var att = { result: success? 'S':'F' };

    return att;
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function get_door(test)
{
    return get_element_in_table(table_d, test);
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function update_door(doors, doorid, door)
{
    var index = -1;
    for(var di in doors) {
        const dor = doors[di];
        if (doorid === dor.id) {
            door.id = dor.id;
            door.direction = dor.direction;
            index = di;
            continue;
        }

        if (!dor.type) {
            door.id = dor.id;
            door.direction = dor.direction;
            index = di;
            continue;
        }
    }
    if (index === -1) {
        door.direction = "?";
        var max_id = -1;
        for(di of doors) {
            if (di.id > max_id) max_id = di.id;
        }
        door.id = max_id+1;
    }
    door.index = index;
    return door;
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function rm_door(doors, door)
{
    var d = [];
    for(var dd of doors) {
        if (dd.id !== door.id) {
            const dc = JSON.parse(JSON.stringify(dd));
            d.push(dc);
        }
    }
    return d;
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function rm_find(finds, find)
{
    var d = [];
    for(var dd of finds) {
        if (dd.id !== find.id) {
            const dc = JSON.parse(JSON.stringify(dd));
            d.push(dc);
        }
    }
    return d;
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function get_area_find_modifier(area)
{
    var mod = 0;
    if (area.color === 'yellow') mod = 0;
    if (area.color === 'red') mod = 10;
    if (area.color === 'green') mod = 5;
    if (area.color === 'blue') mod = 20;
    return mod;
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function get_element_in_table(table, test)
{
    // get and copy door from table
    const mod = (test.dices[0].mod ? test.dices[0].mod: 0);
    const dice_value = test.dices[0].value+mod; // with modifier
    var dd='';
    for(var d of table.fields) {
        if (dice_value >= d.D100_1 && dice_value <= d.D100_2)
            dd = d;
    }
    // after last or before first
    if (dd === '') {
        if (dice_value > d.D100_2)
            dd = table.fields[table.fields.length-1];
        else
            dd = table.fields[0];

    }
    var element = JSON.parse(JSON.stringify(dd));
    element.dice_value = dice_value;
    return element;
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function get_find(test)
{
    var find = get_element_in_table(table_f, test);
    find.id = find.dice_value;
    return find;
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function get_geographic(test)
{
    var geo = get_element_in_table(table_g, test);
    geo.id = geo.dice_value;
    return geo;
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function new_test(name, dice_max)
{
    const test = {
        id:name,
        state:'init',
        dices:[{dice_max:dice_max}]
    };
    return test;
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function get_weapon(test)
{
    var w = get_element_in_table(table_w, test);
    w.id = uuidv1();
    w.is_in_backback = true;
    return w;
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function get_field_help(props)
{
    var h = '';
    if (props.nohelp) return h;
    if (props.children) {
        h  = (<DiceTestHelp>
                {props.children}
              </DiceTestHelp>);
    }
    else {
        const ch = charac.characteristics[props.f];
        if (ch) {
            if (ch.help) {
                h = (<DiceTestHelp>
                       {ch.help(props)}
                     </DiceTestHelp>);
            }
        }
    }
    return h;
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function get_field_str_value(props)
{
    const c = charac.characteristics[props.f];
    var element = field_get_element(props);
    const value = element[props.f];
    if (c.to_str)
        return c.to_str(value);
    return value;
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function field_get_element(props)
{
    // console.log('field_get_element', props);
    if (props.e) return props.encounter;
    if (props.i) return props.i;
    // default
    return props.adventurer;    
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function field_set_element(props, element)
{
    // console.log('field_set_element', props, element);
    if (props.e) return props.set('encounter', element);
    if (props.i) return props.set_items(element);
    // default 
    return props.set('adventurer', element);
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function get_field_adj(props)
{
    var adj='';
    const element = field_get_element(props);
    const field_adj = props.f+'_adj';
    if (!(field_adj in element)) return adj;

    const v = parseInt(element[props.f],10) + parseInt(element[field_adj],10);
    const st1 = { width: '2ch', textAlign:'center'};
    const st2 = { width: '4ch', textAlign:'center'};

    var before = (<span style={st1}>{'+'} {' '}</span>);
    var after =  (<span style={st2}>{' = '} {v}</span>);

    adj = (
        <Grid item>
          <Field4
            {... props}
            nolabel
            before={before}
            after={after}
            vw={'3ch'}
            f={field_adj}>
          </Field4>
        </Grid>
    );


    return adj;
}
// ----------------------------------------------------------------------------

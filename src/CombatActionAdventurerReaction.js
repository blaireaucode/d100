/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import DiceTest from './DiceTest.js';
import * as ch from './combat_helpers.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class CombatActionAdventurerReaction extends Component {

    constructor(props) {
        super(props);
        this.onAttackCompleted = this.onAttackCompleted.bind(this);
        this.onAttackStarted = this.onAttackStarted.bind(this);
    }
    
    onAttackStarted() {
        return ch.new_test('test_adv_att', 100);
    }
    
    onAttackCompleted(test) {
        // FIXME -> save previous attack in history
        const att = ch.get_adv_attack(this.props.adventurer, test);
        var dd = ch.set_combat_adv_attack(this.props.dungeon, att);
        
        // Next combat step
        if (att.result === 'S') // Success ! 
            dd = ch.set_combat_next_step('step_adv_dmg');
        else
            // Fail ! 
            // FIXME DEBUG <---- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            dd = ch.set_combat_next_step('step_adv_dmg');

        // status 
        const s = (att.result === 'S' ? 'Success':'Fail');
        dd = ch.set_combat_status(dd, s);
        this.props.dungeon_set(dd);        
    }
    

    render() {
        
        // FIXME  <---- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        // const enc_reac = combat.enc_reac;
        // if (enc_reac.dice_value >7) return this.renderMonsterEscape(combat);

        return (
            <div className='dungeon'>         
              Choose your action: <br/>
              <li>
                <DiceTest
                  id='test_adv_att'
                  onRollingStarted={this.onAttackStarted}
                  onRollingCompleted={this.onAttackCompleted}>
                  Attack with Left Hand
                </DiceTest> {' '}
                <DiceTest
                  id='test_adv_att'
                  onRollingStarted={this.onAttackStarted}
                  onRollingCompleted={this.onAttackCompleted}>
                  Attack with Right Hand
                </DiceTest>
              </li>
              <li> CastSpell </li>
              <li> ChangeEquippedItem </li>
              <li> UseBeltItem </li>
            </div>
        );
    }

    renderMonsterEscape(combat) {
        return (
            <div className='dungeon'>         
              escape
            </div>
        );
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(CombatActionAdventurerReaction);


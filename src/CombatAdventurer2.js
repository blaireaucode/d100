/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Field4 from './Field4.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class CombatAdventurer2 extends Component {

    render() {
        const adv = this.props.adventurer;
        if (!adv) return ('');

        const tp = { lw:'15ch', vw:'5ch', short_label:true };
        
        return (
            <div className='dungeon'>

              <Field4 nolabel f='name'/>
              <Field4 {...tp} f='hp'/>
              <Field4 {...tp} f='str'/>
              <Field4 {...tp} f='dex'/>
              <Field4 {...tp} f='int'/>
              <Field4 {...tp} f='armour'/>
              <Field4 {...tp} f='shield'/>
              <Field4 {...tp} f='def'/>
              <Field4 {...tp} f='dmg'/>

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CombatAdventurer2);

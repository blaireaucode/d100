/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Field4 from './Field4.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class CombatEncounter2 extends Component {

    render() {
        const e = this.props.encounter;
        if (e && !('name' in e)) return ('No encounter');

        // before
        var n = '['+e.D100_1+'-'+e.D100_2+'] ';
        if (e.D100_1 === e.D100_2) n = '['+e.D100_1+'] ';

        // Label width and value width
        const lw1 = '10ch';
        const vw1 = '4ch';
        const p = {lw:lw1, vw:vw1, e:true, short_label:true};
        
        return (
            <div className='dungeon'>
              
              <Field4 e nolabel before={n} lw={'0ch'} vw={'20ch'} f='name' />

              <Field4 {...p} f='hp'>
                Health Points ({e.hp}).                
              </Field4>
              
              <Field4 {... p} f='av'/>              
              <Field4 {... p} f='def'/>              
              <Field4 {... p} f='dmg'/>
              
              <Field4 {... p} vw={'auto'} f='abilities'/>

              <Field4 {... p} vw={'auto'} f='loot'/>

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CombatEncounter2);



/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import IconButton from '@material-ui/core/IconButton';
import DungeonNewDoor from './DungeonNewDoor.js';
import Cancel from '@material-ui/icons/Cancel';
import update from 'immutability-helper';
import * as dh from './d100_helpers.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class DungeonAreaDoor extends Component {
    
    handleClick(event) {
        // remove a door
        const d = dh.rm_door(this.props.area.doors, this.props.door);
        const aa = update(this.props.area, {doors: {$set:d}});
        this.props.set('area', aa);
    }
    
    render() {
        const door = this.props.door;
        if (!door) return ('');
        var cn = 'dice-test-help';
        var dir='';
        if (door.direction) dir = door.direction;
        if (dir === '?') dir ='';
        var desc ='';
        if (door.dice_value) {
            desc = (<span>
                     {' '} [{door.dice_value}]
                     - {door.type} {door.details} {' '} 
                   </span>);
        }
        return (
            <span>              
              <IconButton onClick={this.handleClick.bind(this)}>
                <Cancel className={cn} />
              </IconButton>
              {' '}
              Door {door.id} {dir}{desc}
              {' '}
              <DungeonNewDoor doorid={door.id}>
                Roll door
              </DungeonNewDoor>
            </span>
        );
    }    
}

export default connect(mapStateToProps, mapDispatchToProps)(DungeonAreaDoor);


/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class DungeonAreaExit extends Component {
   
    render() {
        const exit = this.props.exit;
        if (!exit) return ('');
        return (
            <span>              
              Exit at {exit.direction} {exit.is_door?'(door)':''}
            </span>
        );
    }    
}

export default connect(mapStateToProps, mapDispatchToProps)(DungeonAreaExit);

/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

const default_encounter = {
    name: 'monster',
    D100_1: 0,
    D100_2: 0,
    type: 'h',
    av: 15,
    // av_adj: 0,
    def: 0,
    dmg: 0,
    hp: 5,
    loot: "[K:Nothing]",
    abilities: "Pack, Armoured"
};

export default default_encounter;


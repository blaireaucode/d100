
/*
 * Copyright 2018
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import update from 'immutability-helper';

export function set(state, element_name, value)
{
    // console.log('Store set ', element_name, value);
    global.localStorage.setItem(element_name, JSON.stringify(value));
    const s = {...state, [element_name]:value};
    // console.log('stored state', s);
    return s;
}


export function set_items(state, item)
{
    var index = state.items.findIndex(function(myitem) {
        return myitem.id == item.id; });
    const items = update(state.items, {[index]:{$set:item}});    
    global.localStorage.setItem('items', JSON.stringify(items));
    const s = update(state, {items:{$set:items}});
    return s;
}


export function adventurer_set(state, adventurer)
{
    // console.log('Store set adv: ', adventurer);
    global.localStorage.setItem("adventurer", JSON.stringify(adventurer));
    return {...state, adventurer:adventurer};
}


export function encounter_set(state, encounter)
{
    // console.log('Store set adv: ', encounter);
    global.localStorage.setItem("encounter", JSON.stringify(encounter));
    return {...state, encounter:encounter};
}


export function dungeon_set(state, dungeon)
{
    // console.log('Store set dungeon: ', dungeon);
    global.localStorage.setItem("dungeon", JSON.stringify(dungeon));
    return {...state, dungeon:dungeon};
}


export function test_set(state, id, test)
{
    // console.log('Store set tests: ', id, test);
    const t = update(state.tests, {[id]:{$set:test}});
    global.localStorage.setItem("tests", JSON.stringify(t));    
    return {...state, tests:t};
}

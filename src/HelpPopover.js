/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Popover from '@material-ui/core/Popover';
import IconButton from '@material-ui/core/IconButton';
import { connect } from 'react-redux';
import L from './L.js';
import Info from '@material-ui/icons/InfoOutlined';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class HelpPopover extends Component {

    constructor(props) {
        super(props);
        this.state = { anchorEl:null };
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(event) {
        this.setState( {anchorEl:
                        this.state.anchorEl ? null : event.currentTarget} );
    }
    
    render() {     
        const open = Boolean(this.state.anchorEl);

        const l = (this.props.l?
                   <L onClick={this.handleClick}>
                     {this.props.l} Table
                   </L>
                   :
                   <IconButton onClick={this.handleClick}>
                     <Info className={'dice-test-help'} />
                   </IconButton>);

        return (
            <span>
              {l}
              <Popover
                open={open}
                anchorEl={this.state.anchorEl} 
                onClose={this.handleClick}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}>
                <div
                  className={'help-popover'}
                  style={{maxWidth:this.props.width}}>
                  {this.props.children}            
                </div>                
              </Popover>              
            </span>
        );
    }
    
}

export default connect(mapStateToProps, mapDispatchToProps)(HelpPopover);

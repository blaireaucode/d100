/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import DiceTest from './DiceTest.js';
import DiceTestHelp from './DiceTestHelp.js';
import * as ch from './combat_helpers.js';
import update from 'immutability-helper';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class CombatActionAdvAttack extends Component {

    constructor(props) {
        super(props);
        this.onAttackCompleted = this.onAttackCompleted.bind(this);
        this.onAttackStarted = this.onAttackStarted.bind(this);
    }
    
    onAttackStarted() {
        var test = ch.new_test('test_adv_att', 100);

        const adv = this.props.adventurer;
        const c = adv.str+adv.str_adj;
        test.success_if_below = c;
        
        return test;
    }
    
    onAttackCompleted(test) {
        console.log('test', test);
        const v = parseInt(test.dices[0].value,10);
        const adv = this.props.adventurer;
        const c = test.success_if_below;
        var log = adv.name+' attacks and get '+v+'';
        if (v<=c) {
            log += ' (below '+c+') : success !';
        }
        else {
            log += ' (above '+c+') : fail.';
        }
        const l = update(this.props.log, {$push:[log]});
        this.props.set('log', l);

        // // FIXME -> save previous attack in history
        // const att = ch.get_adv_attack(this.props.adventurer, test);
        // var dd = ch.set_combat_adv_attack(this.props.dungeon, att);
        
        // // Next combat step
        // if (att.result === 'S') // Success ! 
        //     dd = ch.set_combat_next_step('step_adv_dmg');
        // else
        //     // Fail ! 
        //     // FIXME DEBUG <---- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        //     dd = ch.set_combat_next_step('step_adv_dmg');

        // // status 
        // const s = (att.result === 'S' ? 'Success':'Fail');
        // dd = ch.set_combat_status(dd, s);
        // this.props.dungeon_set(dd);        
    }
    

    render() {
        
        // FIXME  <---- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        // const enc_reac = combat.enc_reac;
        // if (enc_reac.dice_value >7) return this.renderMonsterEscape(combat);

        return (
            <span>
              <DiceTestHelp id={'test_adv_att'}>

                To attack a Monster, rolls 1D100.  If you score
                equal to or below the Adventurer's Str or Dex,
                depending on the weapon being used, you hit the Monster.

                <br/>

                Hand Weapons (H) use Str, whilst Ranged Weapons (R)
                use Dex to hit the Monster. If the Adventurer has two
                weapons equipped, either may be used, but not both.

                <br/>

                If the result scores a hit go to 'Roll damage to monster'.
                
              </DiceTestHelp> {' '}
              <DiceTest
                id='test_adv_att'
                onRollingStarted={this.onAttackStarted}
                onRollingCompleted={this.onAttackCompleted}>
                {this.props.adventurer.name} attack (Str)
              </DiceTest>
            </span>
        );
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(CombatActionAdvAttack);


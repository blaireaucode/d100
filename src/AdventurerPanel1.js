/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Field4 from './Field4.js';
import ExpTrack from './ExpTrack.js';
import Grid from '@material-ui/core/Grid';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';


function gg(a,b) {
    return (<Grid
              container
              direction="row"
              spacing={0}
              justify="flex-start"
              alignItems="flex-start">
              <Grid item> {a} </Grid>
              <Grid item> {b} </Grid>
            </Grid>);
}

class AdventurerPanel1 extends Component {

    render() {
        const adv = this.props.adventurer;
        if (!adv) return ('');

        //console.log('adv', this.props.adventurer);
        // Label width and value width
        const lw1 = '11ch';
        const vw1 = '12ch';
        const lw2 = '13ch';
        const vw2 = '4ch';

        const g = {container:true,
                   direction:"row",
                   spacing:3,
                   justify:"flex-start",
                   alignItems:"flex-start"};
        return (
            <div>
              <Grid {...g}>

                <Grid item>
                  <Field4 lw={lw1} vw={vw1} nolabel f='name'/>
                  <Field4 lw={lw1} vw={vw1} f='hero_path'/>
                  <Field4 lw={lw1} vw={vw1} f='race'/>
                </Grid>

                <Grid item>
                  <Field4 lw={lw2} vw={vw2} f='rep'/>
                  <Field4 lw={lw2} vw={vw2} f='fate'/>
                  <Field4 lw={lw2} vw={vw2} f='life'/>
                  <Field4 lw={lw2} vw={vw2} f='gp'/>
                </Grid>

                <Grid item>
                  
                  {gg(<Field4 lw={lw2} short_label vw={vw2} f='hp'/>,'')}
                  
                  {gg(<Field4 lw={lw2} vw={vw2} f='str'/>,
                      <ExpTrack f='str_exp'/>)}

                  {gg(<Field4 lw={lw2} vw={vw2} f='dex'/>,
                      <ExpTrack f='dex_exp'/>)}

                  {gg(<Field4 lw={lw2} vw={vw2} f='int'/>,
                      <ExpTrack f='int_exp'/>)}

                </Grid>
              </Grid>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AdventurerPanel1);

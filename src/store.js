/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import { createStore } from 'redux';
import * as at from './action-types.js';
import * as gr from './action-reducers.js';

const store = setupStore();
export default store;

export function setupStore()
{
    // read from local store (if exist)
    const elements = ['adventurer',
                      'encounter',
                      'dungeon',
                      'area',
                      'quest',
                      'items',
                      'tests',
                      'log'];
    const default_elem = {
        'adventurer': {},
        'encounter': {},
        'dungeon': {},
        'area': {},
        'quest': {},
        'items': [],
        'tests': {},
        'log': []};

    var initialState = {};
    for(var elem of elements) {
        const e =  JSON.parse(global.localStorage.getItem(elem))
              || default_elem[elem];
        initialState[elem] = e;
    }    
    console.log('LocalStorage read', initialState);
    
    // list of action
    const rootReducer = (state = initialState, action) => {
        switch (action.type) {
        case at.SET:
            return gr.set(state, action.element_name, action.value);
        case at.SET_ITEMS:
            return gr.set_items(state, action.items);
        case at.ADVENTURER_SET:
            return gr.adventurer_set(state, action.adventurer);
        case at.ENCOUNTER_SET:
            return gr.encounter_set(state, action.encounter);
        case at.DUNGEON_SET:
            return gr.dungeon_set(state, action.dungeon);
        case at.TEST_SET:
            return gr.test_set(state, action.id, action.test);
        default:
            return state;
        }
    };

    const s = createStore(rootReducer);
    return s;
}


/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import DiceTest from './DiceTest.js';
import Grid from '@material-ui/core/Grid';
import update from 'immutability-helper';
import * as ch from './combat_helpers.js';

import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class DicePanel2 extends Component {

    onCompleted() {
        const id = this.props.tests['current_id'];
        const t = this.props.tests[id];
        var log = '';
        for(var d of t.dices) {
            log += 'Roll D'+d.dice_max+
                ' and get '+d.value;
        }
        const l = update(this.props.log, {$push:[log]});
        this.props.set('log', l);
    }
    
    onD100() {
        return ch.new_test('d100', 100);
    }
    
    onD10() {
        return ch.new_test('d10', 10);
    }
    
    onD6() {
        return ch.new_test('d6', 6);
    }    
    
    render() {
        
        return (
              <Grid
                container
                direction="row"
                justify="flex-start"
                alignItems="center">
                <Grid item xs={2}>    
                  <DiceTest
                    id='d100'
                    onRollingCompleted={this.onCompleted.bind(this)}
                    onRollingStarted={this.onD100}>
                    D100
                  </DiceTest>
                </Grid>
                <Grid item xs={2}>    
                  <DiceTest
                    id='d10'
                    onRollingCompleted={this.onCompleted.bind(this)}
                    onRollingStarted={this.onD10}>
                    D10
                  </DiceTest>
                </Grid>
                <Grid item xs={2}>    
                  <DiceTest
                    id='d6'
                    onRollingCompleted={this.onCompleted.bind(this)}
                    onRollingStarted={this.onD6}>
                    D6
                  </DiceTest>                  
                </Grid>
              </Grid>
        );
    }    
}

export default connect(mapStateToProps, mapDispatchToProps)(DicePanel2);


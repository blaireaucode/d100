/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import DungeonAreaExit from './DungeonAreaExit.js';
import DungeonAreaDoor from './DungeonAreaDoor.js';
import DungeonAreaFinding from './DungeonAreaFinding.js';
import DungeonAreaGeographic from './DungeonAreaGeographic.js';
import * as dh from './d100_helpers.js';
import Grid from '@material-ui/core/Grid';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

const img = dh.build_map_img();

class DungeonArea extends Component {
    
    render() {
        const a = this.props.area;
        // console.log('area', this.props.area, this.props);
        if (!a) return '';
        if (!a.exits) return '';

        // Create the exits components
        var exits = [];
        var i=0;
        for(var e of a.exits) {
            exits.push(<li key={i}><DungeonAreaExit exit={e}/></li>);
            i = i+1;
        }

        // Create the doors components
        var doors = [];
        i=0;
        for( e of a.doors) {
            doors.push(<span key={i}><DungeonAreaDoor door={e}/><br/></span>);
            i = i+1;
        }

        // Create the findings components
        var findings = [];
        i=0;
        for( e of a.findings) {
            findings.push(<span key={i}><DungeonAreaFinding find={e}/><br/></span>);
            i = i+1;
        }

        // Create the geographics components
        var geographics = [];
        i=0;
        for( e of a.geographics) {
            geographics.push(<span key={i}><DungeonAreaGeographic find={e}/><br/></span>);
            i = i+1;
        }

        // get the image
        var src = img[a.id].src;
        return (
            <div className='area'>
              <Grid
                container
                direction="row"
                justify="flex-start"
                alignItems="center">
                
                <Grid item xs={3}>
                  <div>
                    Area {a.id} ({a.color})
                    {/* <p/> */}
                    
                    <img className={'map-img'}
                         src={src}
                         width={400}
                         alt={src}
            /* style={{transform: `rotate(${rotation}deg)`}} */
            /* onClick={() => this.on_area_click([c,r])} */
                    />
                    {' '}
                  </div>
                </Grid>
                <Grid item  xs={6}>
                  {exits}
                  <br/>
                  {doors}
                  <br/>
                  {findings}
                  <br/>
                  {geographics}
                </Grid>
              </Grid>
            </div>
        );
    }

    render_no_dungeon() {
        return('');
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(DungeonArea);

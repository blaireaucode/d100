/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import update from 'immutability-helper';
import Popover from '@material-ui/core/Popover';
import DicePanel from './DicePanel.js';
import L from './L.js';
import * as dh from './d100_helpers.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class DiceTest extends Component {

    constructor(props) {
        super(props);
        this.onClickStartRolling = this.onClickStartRolling.bind(this);
        this.onRollingCompleted = this.onRollingCompleted.bind(this);
        this.state = { anchorEl:null };
    }
    
    onClickStartRolling(event) {
        const test = this.props.onRollingStarted();
        const id = test.id;
        this.props.test_set('current_id', id);
        var tt = dh.test_roll(test);

        // success or fail ?
        if (tt.success_if_below) {
            const v = parseInt(tt.dices[0].value,10);
            if (v<=tt.success_if_below) {
                tt = update(tt, {success:{$set:true}});
            }
            else {
                tt = update(tt, {success:{$set:false}});
            }
        }
        
        this.props.test_set(id, tt);

        // Open the popover
        this.setState( {anchorEl: this.state.anchorEl ?
                        null : event.currentTarget} );
    }
    
    onRollingCompleted() {
        // set test if over
        const id = this.props.id;
        const test = this.props.tests[id];
        var tt = update(test, {state:{$set:'init'}});
        
        // callback
        this.props.test_set(id, tt);
        if (this.props.onRollingCompleted) this.props.onRollingCompleted(tt);

        // close the popover
        this.setState( {anchorEl: null} );
    }
    
    
    render() {
        const id = this.props.id;
        const cid = this.props.tests['current_id'];
        if (id !== cid) {            
            // If this is the current test is not our and is running,
            // we hide the link            
            const test = this.props.tests[cid];
            if (test && test.state !== 'init') return '';
        }
        
        //const test = this.props.tests[id];  
        const open = Boolean(this.state.anchorEl);
        const w = document.documentElement.clientWidth;
        const h = document.documentElement.clientHeight;
        
        return (
            <span>
              <Popover
                open={open}
                anchorEl={this.state.anchorEl}
                anchorReference="anchorPosition"
                anchorPosition={{ top: h/2, left: w/3 }}
                onClose={this.handleClick}
                anchorOrigin={{
                    vertical: 'center',
                    horizontal: 'right',
                }}
                transformOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}>
                <div className={'dice-panel'}>
                  <DicePanel
                    onRollingCompleted={this.onRollingCompleted}
                  />            
                </div>            
              </Popover>
              <L onClick={this.onClickStartRolling}>
                {this.props.children}
              </L>
            </span>
        );
    }
    
}

export default connect(mapStateToProps, mapDispatchToProps)(DiceTest);

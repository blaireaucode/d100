/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import DiceTest from './DiceTest.js';
import update from 'immutability-helper';
import * as dh from './d100_helpers.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class CombatActionEncounterReaction extends Component {

    constructor(props) {
        super(props);
        this.onEncReacTestCompleted = this.onEncReacTestCompleted.bind(this);
        this.onEncReacTestStarted = this.onEncReacTestStarted.bind(this);
    }
    
    onEncReacTestStarted() {
         const test = {
            id:'test_enc_reac',
            state:'init',
            dices:[{dice_max:10}], 
        };
        return test;
    }
    
    onEncReacTestCompleted(test) {
        const e = dh.get_encounter_reaction(test);
        var dd = update(this.props.dungeon,
                        {area: {combat: {enc_reac: {$set:e}}}});
        // Next combat step
        dd = update(dd,
                    {area: {combat: {step: {$set:'step_adv_reac'}}}});
        // status
        const s = e.txt;
        dd = update(dd,
                    {area: {combat: {status: {$set:s}}}});
        this.props.dungeon_set(dd);        
    }
    
    render() {
        const area = this.props.dungeon.area;
        if (!area) return ('');
        const combat = area.combat;
        if (!combat) return ('');
        const step = combat.step;
        if (!step) return ('');

        return (
            <DiceTest
              id='test_enc_reac'
              onRollingStarted={this.onEncReacTestStarted}
              onRollingCompleted={this.onEncReacTestCompleted}>
              Roll to get the monster reaction ...
            </DiceTest>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CombatActionEncounterReaction);


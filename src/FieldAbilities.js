/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import Input from '@material-ui/core/Input';
import update from 'immutability-helper';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';
import * as eh from './encounter_helpers';
import * as dh from './d100_helpers';
import * as charac from './characteristics.js';

class FieldAbilities extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: this.props.encounter.abilities
        };
    }

    onChange = (event) => {
        const v = event.target.value;
        const abilities = eh.string_to_abilities(v);
        this.setState({value:abilities});
        const el = this.props.encounter;
        const ee = update(el,
                          {abilities:{$set:abilities}});
        this.props.set('encounter', ee);
    }

    componentWillReceiveProps(nextProps){
        const v = nextProps.encounter.abilities;
        if (this.state.value !== v) {
            this.setState({value:v});
        }
    }

    render() {
        // Common props
        var inputProps = {
            className: 'fieldEdit',
            disabled: false,
            type: 'text',
            onChange:this.onChange,
        };

        // characteristic
        const c = charac.characteristics['abilities'];

        // Help
        const h = dh.get_field_help(this.props);

        // label
        var label_txt = '';
        if (!this.props.nolabel) {
            label_txt = c.label;
            if (this.props.short_label)
                label_txt = c.short;
        }
        const lw = (this.props.lw ? this.props.lw:2+label_txt.length+'ch');
        const labelStyle = {width: lw, textAlign:'left'};
        
        // abilities
        const at = eh.abilities_to_string(this.state.value);
        const l = at.length>15 ? 15: at.length;
        inputProps.style = {width: l+'em'};
        const ab = <Input
                     disableUnderline={true}
                     inputProps={inputProps}
        // multiline={true}
                     value={at}/>;
        return (
            <Grid
              container
              direction="row"
              justify="flex-start"
              alignItems="flex-start">
              <Grid item style={labelStyle}>
                {h} {label_txt}
              </Grid>
              <Grid item>
                {ab}
              </Grid>
            </Grid>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FieldAbilities);

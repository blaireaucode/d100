
/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import IconButton from '@material-ui/core/IconButton';
import Cancel from '@material-ui/icons/Cancel';
import update from 'immutability-helper';
import * as dh from './d100_helpers.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class DungeonAreaGeographic extends Component {
   
    handleClick(event) {
        // remove a find
        const d = dh.rm_find(this.props.area.geographics, this.props.find);
        const aa = update(this.props.area, {geographics: {$set:d}});
        this.props.set('area', aa);
    }
    
    render() {
        const find = this.props.find;
        if (!find) return ('');
        var cn = 'dice-test-help';
        return (
            <span>              
              <IconButton onClick={this.handleClick.bind(this)}>
                <Cancel className={cn} />
             </IconButton>
              {' '}
              {find.id} {' '} ({find.name}) {' '} {find.details}
              {' '}
            </span>
        );
    }    
}

export default connect(mapStateToProps, mapDispatchToProps)(DungeonAreaGeographic);

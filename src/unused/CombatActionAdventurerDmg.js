/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import DiceTest from './DiceTest.js';
import DiceTestHelp from './DiceTestHelp.js';
import update from 'immutability-helper';
import * as dh from './d100_helpers.js';
import * as ch from './combat_helpers.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class CombatActionAdventurerDmg extends Component {

    constructor(props) {
        super(props);
        this.onDmgCompleted = this.onDmgCompleted.bind(this);
        this.onDmgStarted = this.onDmgStarted.bind(this);
    }

    onDmgStarted() {
        var test = ch.new_test('test_adv_dmg', 6);
        test.dices.push({dice_max:10});
        return test;
    }

    onDmgCompleted(test) {
        const dungeon = this.props.dungeon;
        const combat = dungeon.area.combat;
        const a = combat.adv_att;
        if (!a) return;
        // var att = JSON.parse(JSON.stringify(a)); // FIXME ?

        // FIXME--> to put in helper
        const d10 = test.dices[1].value;
        const d6 = test.dices[0].value;
        const hl = dh.get_hit_location(d10);
        var dmg_mod = hl.DmgMod;
        if (dmg_mod === 'Check') {
            dmg_mod = 0;
            console.log('TODO Belt Check !!!!!!!', dmg_mod);
        }
        else dmg_mod = parseInt(dmg_mod, 10);

        // dmg
        const adv_dgm_mod = 0; // FIXME
        const def = parseInt(combat.encounter.Def, 10);
        var dmg = dmg_mod + d6 + adv_dgm_mod - def;
        if (dmg < 0) dmg = 0;
        const hp = combat.encounter.HP - dmg;
        var dd = update(dungeon,
                          {area: {combat: {encounter: {HP: {$set:hp}}}}});

        // status
        const s = 'Hit location = '+hl.Location+
              '  dmg is '+d6+
              ' + '+dmg_mod+' + '+adv_dgm_mod+
              ' - '+def+' = '+dmg;
        dd = update(dd,
                    {area: {combat: {status: {$set:s}}}});

        // step
        dd = update(dd,
                    {area: {combat: {step: {$set:'step_enc_att'}}}});
        this.props.dungeon_set(dd);
    }


    render() {
        const area = this.props.dungeon.area;
        if (!area) return ('');
        const combat = area.combat;
        if (!combat) return ('');
        const step = combat.step;
        if (!step) return ('');
        const enc_reac = combat.enc_reac;
        if (!enc_reac) return ('');

        // DEBUG 
        //if (enc_reac.dice_value >7) return this.renderMonsterEscape(combat);

        return (
            <span>
              <DiceTest
                id='test_adv_dmg'
                onRollingStarted={this.onDmgStarted}
                onRollingCompleted={this.onDmgCompleted}>
                Success !! Roll to see the damage ... 
              </DiceTest>
              <DiceTestHelp id='test_adv_dmg'>
                
                Roll both the <u>damage die (1d6)</u> and the <u>location die
                (1d10)</u> together. <br/><br/>

                Apply the damage modifier to the <u>damage
                die</u> for the location rolled (see Hit Location
                table) and the Adventurers DMG modifier (if
                any TODO). <br/><br/>
                
                Then deduct the Monster DEF value
                 = {combat.encounter.Def} from the modified damage
                dice. <br/><br/>

                The remaining amount is the number of HP that
                are dealt to the Monster or Monsters if encountering
                more than one (see Monster Ability - Pack)
                
              </DiceTestHelp>
            </span>
        );
    }

    renderMonsterEscape(combat) {

        return (
            <div className='dungeon'>
              escape
            </div>
        );
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(CombatActionAdventurerDmg);

/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import DungeonArea from './DungeonArea.js';
import DiceTestHelp from './DiceTestHelp.js';
import update from 'immutability-helper';
// import DungeonCombat from './DungeonCombat.js';
// import DungeonMove from './DungeonMove.js';
// import DungeonSearch from './DungeonSearch.js';
import DiceTest from './DiceTest.js';
import * as dh from './d100_helpers.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class Dungeon extends Component {

    constructor(props) {
        super(props);
        this.onNewAreaTestCompleted = this.onNewAreaTestCompleted.bind(this);
        this.onNewAreaTestStarted = this.onNewAreaTestStarted.bind(this);
    }
    
    onNewAreaTestStarted() {
        // nothing. Maybe later -> remove current area (?)
        // or change color (dim ?)
        const dd = update(this.props.dungeon, {area: {$set:{}}});
        this.props.dungeon_set(dd);
        const test = {
            id:'new_area',
            state:'init',
            dices:[{dice_max:100}]
        };
        // this.props.test_set('new_area', test);        
        return test;
    }
    
    onNewAreaTestCompleted(test) {
        console.log('onNewAreaTestCompleted', test);
        const a = dh.get_area(test);
        const dd = update(this.props.dungeon, {area: {$set:a}});
        this.props.dungeon_set(dd);
    }
    
    render() {
        const d = this.props.dungeon;
        if (!d.name) return this.render_no_dungeon();

        return (
            <div className='dungeon'>              
              Dungeon: {d.name}
              <p/>
              
              <DungeonArea/>

              <p/>
              
              <DiceTest
                // will be moved to DungeonActions
                id='new_area'
                onRollingStarted={this.onNewAreaTestStarted}
                onRollingCompleted={this.onNewAreaTestCompleted}
              >Move to another room ...</DiceTest>
               <DiceTestHelp id='new_area'>
                
              Roll D100 to get the area from the table_m
                
              </DiceTestHelp>
                            
            </div>
        );
    }

    render_no_dungeon() {
        return(<span>No dungeon yet.</span>);
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(Dungeon);

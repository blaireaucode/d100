/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import EncounterField from './EncounterField.js';
import * as eh from './encounter_helpers.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class CombatEncounter extends Component {

    // FIXME --> loot: link to the exact Tables
    // FIXME --> abilities: parse and display info
    // FIXME --> HP: parse if several values (multiple)
    
    render() {
        const area = this.props.dungeon.area;
        if (!area) return ('');
        const combat = area.combat;
        if (!combat) return ('');
        const e = combat.encounter;
        if (!e) return ('Awaiting encounter');

        //console.log('combat', combat);
        //console.log('encounter', e);

        // type
        const type = eh.encounter_get_typename(e);

        // abilities
        const ab = e.Abilities;//eh.encounter_get_abilities(e);
        var abilities = [];
        for(var a of ab) {
            console.log('ha = ', a.help);
            abilities.push(<div className='help' key={a.name}>
                             <i>{a.name}</i> : {a.help}
                           </div>);
        }
        if (abilities.length === 0)
            abilities = 'This monster has no particular ability.';
        
        return (
            <div className='dungeon'>         
              {e.Monster} {' - '}
              {type} {' - '}
              (<span className='field'>{e.D100}</span>)
              <br/>

              <EncounterField name={'Abilities'}>
                {abilities}
              </EncounterField>
              
              <EncounterField name={'AV'}>
                
                Attack Value. You will have to roll above this value
                ({e.AV} + modifier) to avoid its attack.

              </EncounterField>         
              
              <EncounterField name={'Def'}>

                Defense modifier. When you hit the monster, this value
                ({e.Def}) will be deducted from the damage dice.
                
              </EncounterField>
              
              <EncounterField name={'Dmg'}>

                Damage modifier. When the monster hits you, this value
                ({e.Dmg}) will be added to the damage you get.
                
              </EncounterField>

              <EncounterField name={'HP'}>

                Health Points ({e.HP}). 
                
              </EncounterField>

              <EncounterField name={'Loot'}>

                Indicate the tables you may loot when the monster is
                killed ('K'). For example, [K:Table I/TA] allow you to
                loot in table I (Items) or table TA (Treasure
                A).
                
              </EncounterField>

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CombatEncounter);


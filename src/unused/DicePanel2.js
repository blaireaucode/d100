/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import DicePanel from './DicePanel.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class DicePanel2 extends Component {

    render() {
        
        return (
            // <div>
            //   <Grid
            //     container
            //     direction="row"
            //     justify="flex-start"
            //     alignItems="flex-start">
            //     {/* <Grid item xs={12}> */}
            //     {/*   <DefaultDices/> */}
            //     {/* </Grid> */}
            //     <Grid item xs={12}>
                  <DicePanel/>
            //     </Grid>
            //   </Grid>
            // </div>    
        );
    }    
}

export default connect(mapStateToProps, mapDispatchToProps)(DicePanel2);


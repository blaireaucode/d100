/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class CombatAdventurer extends Component {

    render() {
        const adv = this.props.adventurer;
        if (!adv) return ('');
        return (
            <div className='dungeon'>   
              {adv.name} str={adv.strength} HP={adv.HP}           
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CombatAdventurer);


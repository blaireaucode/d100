/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import CombatEncounter from './CombatEncounter.js';
import CombatAdventurer from './CombatAdventurer.js';
import CombatStatus from './CombatStatus.js';
import CombatActions from './CombatActions.js';
import Grid from '@material-ui/core/Grid';

import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class Combat extends Component {

    render() {
        const area = this.props.dungeon.area;
        if (!area) return this.renderNoCombat();
        const combat = area.combat;
        if (!combat) return this.renderNoCombat();
        
        return (
            <div className='dungeon'>
              <Grid
                container
                direction="row"
                justify="flex-start"
                alignItems="flex-start"
              >
                <Grid item xs={12}>
                  <div className='combat-status'>   
                    Combat step: {combat.step}<p/>
                    {/* TODO : CombatStep */}          
                  </div>
                </Grid>
                
                <Grid item xs={6}>
                  <CombatAdventurer/>
                </Grid>

                <Grid item xs={6}>
                  <CombatEncounter/>
                  <p/>
                </Grid>

                <Grid item xs={12}>
                  <CombatStatus/>
                  <p/>
                </Grid>

                <Grid item xs={12}>
                  <CombatActions/>
                  <p/>
                </Grid>

              </Grid>
            </div>
        );
    }

    renderNoCombat() {
        return (
            <span>
              Enjoy, no combat right now !
            </span>
        );
    }
    
}

export default connect(mapStateToProps, mapDispatchToProps)(Combat);


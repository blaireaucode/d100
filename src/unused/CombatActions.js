/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import CombatActionStart from './CombatActionStart.js';
import CombatActionEncounterReaction from './CombatActionEncounterReaction.js';
import CombatActionEncounterAttack from './CombatActionEncounterAttack.js';
import CombatActionAdventurerReaction from './CombatActionAdventurerReaction.js';
import CombatActionAdventurerDmg from './CombatActionAdventurerDmg.js';
import CombatActionEncounterDmg from './CombatActionEncounterDmg.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class CombatActions extends Component {

    constructor(props) {
        super(props);
        this.map = [];
        this.map['step_start'] = (<CombatActionStart/>);
        this.map['step_enc_reac'] = (<CombatActionEncounterReaction/>);
        this.map['step_adv_reac'] = (<CombatActionAdventurerReaction/>);
        this.map['step_adv_dmg'] = (<CombatActionAdventurerDmg/>);
        this.map['step_enc_att'] = (<CombatActionEncounterAttack/>);
        this.map['step_enc_dmg'] = (<CombatActionEncounterDmg/>);
    }

    render() {
        const area = this.props.dungeon.area;
        if (!area) return ('');
        const combat = area.combat;
        if (!combat) return ('');
        const step = combat.step;
        if (!step) return ('');
        const ac = this.map[step];
        if (!ac) return ('');        
        return (
            <div className='combat-actions'>
              {ac}
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CombatActions);


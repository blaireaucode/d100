/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Field4 from './Field4.js';
import ExpTrack from './ExpTrack.js';
import NewWeapon from './NewWeapon.js';
import Item from './Item.js';
import Grid from '@material-ui/core/Grid';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class AdventurerBackPackItems extends Component {

    render() {
        const adv = this.props.adventurer;
        if (!adv) return ('');
        //console.log('it props', this.props);

        var items = [];
        var i=0;
        for(var item of this.props.items) {
            //console.log(item);
            if (item.is_in_backback)
                items.push(<Item key={i} item={item}/>);
            i+=1;
        }
        
        return (
            <span>

              <p/>
              {items}
              <p/>
              
              <NewWeapon/>
              
            </span>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AdventurerBackPackItems);

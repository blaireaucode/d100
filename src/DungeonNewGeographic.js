/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import DiceTestHelp from './DiceTestHelp.js';
import DiceTest from './DiceTest.js';
import update from 'immutability-helper';
import * as dh from './d100_helpers.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class DungeonNewGeographic extends Component {

    constructor(props) {
        super(props);
        this.onNewGeographicTestCompleted = this.onNewGeographicTestCompleted.bind(this);
        this.onNewGeographicTestStarted = this.onNewGeographicTestStarted.bind(this);       
    }

    onNewGeographicTestStarted() {
        const test = {
            id:'new_geographic',
            state:'init',
            dices:[{dice_max:100}]
        };
        return test;
    }

    onNewGeographicTestCompleted(test) {
        // get a geographic
        var g = dh.get_geographic(test);
        const a = update(this.props.area, {geographics: {$push:[g]}});
        this.props.set('area', a);
        // status
        const s = 'You find geographic n°'+g.dice_value+' '+g.details;
        const l = update(this.props.log, {$push:[s]});
        this.props.set('log', l);
    }

    render() {
        var t = '';
        if (this.props.children) t = this.props.children;
        else t = 'Roll a new geographic';
        return (
            <span>
              <DiceTestHelp id={'new_geographic'+this.testid}>
                Roll D100 to get something from Table G
                {' '}
              </DiceTestHelp>
              {' '}
              <DiceTest
                id={'new_geographic'}
                onRollingStarted={this.onNewGeographicTestStarted}
                onRollingCompleted={this.onNewGeographicTestCompleted}
              > {' '}
                {t}</DiceTest>
            </span>
        );

    }

}

export default connect(mapStateToProps, mapDispatchToProps)(DungeonNewGeographic);

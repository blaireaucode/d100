/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import FieldInput from './FieldInput.js';
import RadioGroup from '@material-ui/core/RadioGroup';
//import Radio from '@material-ui/core/Radio';
import Radio from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';
import update from 'immutability-helper';
import Stars from '@material-ui/icons/Stars';
import * as dh from './d100_helpers';

class ExpTrack extends Component {

    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
    }


    onChange = (event) => {
        var v = parseInt(event.target.value, 10);
        const elem_name = dh.get_character_name(this.props);
        const elem = this.props[elem_name];
        const ee = update(elem,
                          {[this.props.f]:{$set:v}});
        this.props.set(elem_name, ee);
    }

    render() {
        const elem = dh.get_character_name(this.props);
        const value = this.props[elem][this.props.f];//.toString();

        console.log('value ', value);

        const s = { width:'0.8ch', height:'0.8ch'};

        const icon = <span>{/* className={'exptrack-icon'} style={s}> */}
                       <Stars  className={'exptrack-icon'} style={s}/>  
                     </span>;
        const iconc = <span className={'exptrack-checked-icon'} style={s}>
                        {/* <Stars  style={s}/>  */}
                      </span>;
        
        this.rad = [];
        const radio = <Radio
                        icon={icon}
        /* fullWidth */
                        /* sizeSmall */
                        checked={false}
                        value={'0'} key={0}
                        /* style={{ backgroundColor: 'transparent' }}  */

                        onChange={this.onChange} 
        /* checkedIcon={<span className={'exptrack-checked-icon'}/>} */
                      />;
        const fcl = <Grid  key={i}  item>
                      <FormControlLabel
                        key={0} 
                        className={'exptrack-control'}
        /* label={value} */
                        control={radio}/>                      
                    </Grid>
        ;
        //        this.rad.push(radio);
        this.rad.push(radio);

        for(var i=1;i<10;i++) {
            const radio = <IconButton
                            icon={icon}
                            key={i}
                            /* disableHoverListener */
                            checked={(i<=value)}
                            value={i.toString()}
                            onChange={this.onChange} 
                            checkedIcon={iconc}>{icon}</IconButton>;
            const fcl =  <Grid  key={i}  item>
                           <FormControlLabel
                             key={i} 
                             className={'exptrack-control'}
            /* label={value} */
                             control={radio}/>
                         </Grid>
            ;
            // this.rad.push(radio);
            this.rad.push(radio);
            //this.rad.push(<span >{radio}</span>);
        }

        // add label to last one
        // const radio = <Radio
        //                 icon={<span className={'exptrack-icon'}/>}
        //                 checkedIcon={<span className={'exptrack-checked-icon'}/>}/>;
        // this.rad[9] = (
        //     <FormControlLabel
        //       key={10} value={'10'}
        //       className={'exptrack-control'}
        //       label={value}
        //       control={radio}/>);       
        
        return (<span>
                  {/* <Grid */}
                  {/*   container */}
                  {/*   direction="row" */}
                  {/*   spacing={0} */}
                  {/*   justify="flex-start" */}
                  {/*   alignItems="flex-start"> */}
                  
                  {/* <RadioGroup value={value.toString()} onChange={this.onChange} row>  */}
                  {this.rad}
                  {/* </RadioGroup>  */}
                  <br/>
                  {/* </Grid> */}
                </span>);
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ExpTrack);

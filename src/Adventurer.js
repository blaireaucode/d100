/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import AdventurerPanel1 from './AdventurerPanel1.js';
import AdventurerBackPackItems from './AdventurerBackPackItems.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class Adventurer extends Component {
    
    render() {
        const adv = this.props.adventurer;
        if (!adv) return ('');
        // console.log('adv', adv);
        
        return (
            <div>              

              <AdventurerPanel1/>

              {/* <AdventurerPanel2/> */}

              {/* <AdventurerEquippedItems/> */}

              <AdventurerBackPackItems/>
              
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Adventurer);

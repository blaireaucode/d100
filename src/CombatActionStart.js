/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import DiceTest from './DiceTest.js';
import * as ch from './combat_helpers.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class CombatActionStart extends Component {

    constructor(props) {
        super(props);
        this.onNewEncTestCompleted = this.onNewEncTestCompleted.bind(this);
        this.onNewEncTestStarted = this.onNewEncTestStarted.bind(this);
    }
    
    onNewEncTestStarted() {
        // FIXME --> modify according to quest etc ...
        return ch.new_test('test_new_encounter', 100);
    }
    
    onNewEncTestCompleted(test) {
        const e = ch.get_encounter(test);
        var dd = ch.set_combat_encounter(this.props.dungeon, e);
        dd = ch.set_combat_next_step(dd, 'step_enc_reac');
        dd = ch.set_combat_next_round(dd);
        this.props.dungeon_set(dd);        
    }
       
    render() {
        return (
            <span className='dungeon'>         
              <DiceTest
                id='test_new_encounter'
                onRollingStarted={this.onNewEncTestStarted}
                onRollingCompleted={this.onNewEncTestCompleted}>
                Roll to see what is in this area ...
              </DiceTest>
            </span>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CombatActionStart);


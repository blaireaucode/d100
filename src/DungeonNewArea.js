/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import DiceTestHelp from './DiceTestHelp.js';
import DiceTest from './DiceTest.js';
import update from 'immutability-helper';
import * as dh from './d100_helpers.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class DungeonNewArea extends Component {

    constructor(props) {
        super(props);
        this.onNewAreaTestCompleted = this.onNewAreaTestCompleted.bind(this);
        this.onNewAreaTestStarted = this.onNewAreaTestStarted.bind(this);
    }

    onNewAreaTestStarted() {
        // nothing. Maybe later -> remove current area (?)
        // or change color (dim ?)
        const test = {
            id:'new_area',
            state:'init',
            dices:[{dice_max:100}]
        };
        return test;
    }

    onNewAreaTestCompleted(test) {
        const a = dh.get_area(test);
        this.props.set('area', a);
        // status
        const s = 'You move to area n°'+a.id+
              ' ('+a.color+')';
        const l = update(this.props.log, {$push:[s]});
        this.props.set('log', l);
    }

    render() {
        return (
            <span>
              <DiceTestHelp id='new_area'>
                Roll D100 to get the area from the Table M
              {' '}
              </DiceTestHelp>
              {' '}
              <DiceTest
                id='new_area'
                onRollingStarted={this.onNewAreaTestStarted}
                onRollingCompleted={this.onNewAreaTestCompleted}
              > {' '}
                Move to another room</DiceTest>
            </span>
        );

    }

}

export default connect(mapStateToProps, mapDispatchToProps)(DungeonNewArea);

/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import DungeonArea from './DungeonArea.js';
import DungeonNewArea from './DungeonNewArea.js';
import DungeonNewDoor from './DungeonNewDoor.js';
import DungeonNewFind from './DungeonNewFind.js';
import DungeonNewGeographic from './DungeonNewGeographic.js';
import LogPanel from './LogPanel.js';
import Grid from '@material-ui/core/Grid';
import DefaultDices from './DefaultDices.js';
import Table from './Table.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class Dungeon extends Component {
    
    render() {
        //const s = 3;        
        return (
            <div className='combat-layout'>
              <Grid
                container
                direction="row"
                justify="space-evenly"
                alignItems="flex-start">
                
                <Grid xs={12} item className='combat-layout'>
                  <DungeonArea/>
                </Grid>

                
              </Grid>
              <p/>
              <Grid
                container
                direction="row"
                justify="flex-start"
                alignItems="flex-start">                
                {/*  <Grid item xs={12} className='combat-layout-actions'> */}

                <Grid item xs={2}>
                  <Table t={'doors'}/>
                </Grid>

                <Grid item xs={2}>
                  <Table t={'find'}/>
                </Grid>

                <Grid item xs={2}>
                  <Table t={'geographic'}/>
                </Grid>

                {/* <Grid item xs={s}> */}
                {/*   <Table t={'mapping'}/> */}
                {/* </Grid> */}

                <Grid item xs={3}>
                  <DefaultDices/>
                </Grid>

                <Grid item xs={3}>
                  <DungeonNewArea/>
                </Grid>
                
                <Grid item xs={12}>
                  <DungeonNewDoor doorid={'new'}/>
                </Grid>
                
                <Grid item xs={12}>
                  <DungeonNewFind/>
                </Grid>
                
                <Grid item xs={12}>
                  <DungeonNewGeographic/>
                </Grid>
                
                <Grid item xs={12}>
                  <LogPanel/>
                </Grid>
                
              </Grid>                
              {/* </Grid> */}
            </div>
        );
        
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(Dungeon);

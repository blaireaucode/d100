/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class DungeonMove extends Component {
   
    render() {
        const d = this.props.dungeon;
        console.log('DungeonMove', d);
        if (!d.name) return this.render_no_dungeon();
        return (
            <div>              
              DungeonMove {d.name}
              <p/>

           
              
            </div>
        );
    }

    render_no_dungeon() {
        return(<span>No dungeon currently.</span>);
    }
    
}

export default connect(mapStateToProps, mapDispatchToProps)(DungeonMove);

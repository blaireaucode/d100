
/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { RIEInput, RIENumber} from '@attently/riek';
import DiceTestHelp from './DiceTestHelp.js';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Input from '@material-ui/core/Input';
import update from 'immutability-helper';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class Field2 extends Component {

    constructor(props) {
        super(props);
        const s = this.props.struct || this.props.adventurer;
        this.state = {
            value: s[this.props.name]
        };
    }

    onChange = (newState) => {
        console.log('Field2 change', this.state, newState);
        //const struct = this.props.struct;
        const struct = this.props.struct || this.props.adventurer;
        const n = this.props.name;
        var v = newState.value;
        if (this.props.type === 'n')
            v = parseInt(v, 10);
        this.setState(newState);
        const a = update(struct, {[n]:{$set:v}});

        // determine if it is adv or enc
        if ("AV" in struct) {
            console.log('AV does exist -> enc', struct);
            const dungeon = this.props.dungeon;
            var dd = update(dungeon,
                            {area: {combat: {encounter: {$set:a}}}});
            this.props.dungeon_set(dd);            
        }
        else {
            console.log('AV NOT  -> adv', struct);
            this.props.adventurer_set(a);
        }
    };

    validateInt(a) {
        // console.log('validate', a, this.state);
        if (a === '') return false;
        return true;
    } 
    
    render() {
        console.log('Field2 render', this.props);
        var input = '';
        const isDisabled = this.props.ro === true;

        const inputProps = {
            className: 'field',
            disabled: true,
            value:this.state.value
        };
        
        if (this.props.type === 'n') {
            input = (
                <RIENumber
                  classEditing={'fieldEdit'}
                  className={'field'}
                  isDisabled={isDisabled}
                  value={this.state.value}
                  change={this.onChange}
                  validate={this.validateInt.bind(this)}
                  propName='value'
                />);
        }
        else {
            // input = (
            //     <RIEInput
            //       classEditing={'fieldEdit'}
            //       className={'field'}
            //       isDisabled={isDisabled}
            //       value={this.state.value}
            //       change={this.onChange}
            //       propName='value'
            //     />);
            input = (
                <Input
                  id="time"
                  disableUnderline={true}
                  inputProps={inputProps}
                />
            );
        }

        return (
            <Grid
              container
              direction="row"
              justify="flex-start"
              alignItems="flex-start">
              <Grid item xs={6}>
                {this.props.name} {' '}
                <DiceTestHelp id='adv_dmg'>
                  help
                </DiceTestHelp>
              </Grid>
              <Grid item xs={6}>
                {input}
              </Grid>
            </Grid>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Field2);


/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import DiceTestHelp from './DiceTestHelp.js';
import Grid from '@material-ui/core/Grid';
import Input from '@material-ui/core/Input';
import update from 'immutability-helper';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';
import * as charac from './characteristics.js';
import * as lh from './labels_helpers';

class Field3 extends Component {

    constructor(props) {
        super(props);
        var element = 'adventurer';
        if (this.props.e) element = 'encounter';
        if (this.props.o) element = this.props.o;
        const fadj = this.props.f+'_adj';
        var adj = false;
        const el = this.props[element];        
        if (el && fadj in el) adj = fadj;
        this.state = {
            element: element,
            adj: (adj ? fadj:false),
            value: el? this.props[element][this.props.f]:'* error *',
        };
        // this.state.value = this.getValue(this.state.value);
        // console.log('state',this.state);
    }

    onChange = (event) => {
        var v = event.target.value;
        if (this.props.type === 'number')
            v = parseInt(v, 10);
        this.setState({value:v});
        const p = this.props.f;
        const el = this.props[this.state.element];
        // console.log('pro', this.props, this.state);
        const ee = update(el,
                          {[p]:{$set:v}});
        this.props.set(this.state.element, ee);
    }

    componentWillReceiveProps(nextProps){
        //console.log('next props ', this.state, nextProps);
        const el = nextProps[this.state.element];        
        if (!el) return;
        const v = el[this.props.f];
        if (this.state.value !== v) {
            this.setState({value:v});
            // console.log('this.state', this.state);
        }
    }

    render() {

        // Type
        const t = this.props.type;

        // Width
        const w = t === 'number' ? '3em':'';

        const align = t === 'number' ? 'right':'left';
        // console.log('align', this.props.f, align);

        // Common props
        const inputProps = {
            className: 'fieldEdit',
            disabled: false,
            type: t || 'text',
            value:this.state.value,
            onChange:this.onChange,
            style: {width: w, textAlign:align}
        };
        // console.log('inputProps', inputProps);

        // Help
        var h = '';
        if (this.props.children) {
            h  = (<DiceTestHelp>
                    {this.props.children}
                  </DiceTestHelp>);
        }
        else {
            const ch = charac.characteristics[this.props.f];
            if (ch) {
                if (ch.help) {
                    h = (<DiceTestHelp>
                           {ch.help(this.props)}
                         </DiceTestHelp>);                     
                }              
            }
        }

        // Label
        var label = lh.get_label(this.props.f).short;
        const style = this.props.style || {width:'6em'};
        label=(
            <Grid item style={style}>
              {h}
              {label} {' '}
            </Grid>
        );
        if (this.props.nolabel) label='';

        // Adjusted value
        var adj = '';
        if (this.state.adj) {
            adj = ( 
                <Grid item>
                  <Field3
                    {... this.props}
                    nolabel
                    margin
                    sign
                    f={this.state.adj}
                    type='number'>
                  </Field3>
                </Grid>
            );
        }

        // Offset 
        var m = '';
        if (this.props.margin)
            m = (<span>&nbsp;</span>);

        // Sign ?
        var sign='';
        if (this.props.sign) {
            if (this.state.value >= 0)
                sign=(<span className='fieldSign'>+</span>);
            else
                sign=(<span className='fieldSign'>+</span>);
        }

        // Total
        var total ='';
        if (this.state.adj) {
            const s = this.props.e ? this.props.encounter:this.props.adventurer;
            const t = this.state.value + s[this.state.adj];
            total = (<span className='fieldSign'> = {t} </span>);
        }
        
        return (
            <Grid
              container
              direction="row"
              justify="flex-start"
              alignItems="flex-start">
              {label}
              <Grid item>
                {this.props.before}
                {m}
                {sign}
                <Input
                  disableUnderline={true}
                  inputProps={inputProps}
            /* startAdornment={sign} */
                />
              </Grid>
              {adj}
              <Grid item>
                {total}
              </Grid>
            </Grid>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Field3);

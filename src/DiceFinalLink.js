/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from './L.js';

class DiceFinalLink extends Component {

    constructor(props) {
        super(props);
        this.onTimeout = this.onTimeout.bind(this);
        this.state = { done:false};
        this.willUnmount = false;
    }

    onTimeout(event) {
        if (!this.willUnmount)
            this.setState({done:true});
    }

    componentWillUnmount() {
        this.willUnmount = true;
    }

    render() {
        const test = this.props.test;
        //console.log('final lin state', this.state, this.props.delay);
        var cn = 'hidden-link';
        if (this.state.done) cn = '';
        else {
            const delay = this.props.delay; // FIXME put delay as parameters in store
            this.interval = setTimeout(() => { this.onTimeout();}, delay);
        }

        var s='';
        for(var d of test.dices) {
            s = s+' '+d.value;
            if (d.mod) {
                const t = d.mod+d.value;
                s = s+'+'+d.mod+' = '+t;
            }
        }
        if (!test.success_if_below) {
            return (
                <L className={cn} onClick={this.props.onClickEnd}>
                  You roll {s}
                </L>
            );
        }
        else {
            if (test.success)
                return (
                    <L className={cn} onClick={this.props.onClickEnd}>
                      You roll {s}, success ! 
                    </L>
                );
            else
                return (
                    <L className={cn} onClick={this.props.onClickEnd}>
                      You roll {s}, test failed.
                    </L>
                );
        }
    }
}

export default DiceFinalLink;

/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

const default_adventurer = {
    name: 'toto',

    str: 40,
    dex: 30,
    int: 20,
    str_adj: 1,
    dex_adj: 2,
    int_adj: 3,
    
    armour: 0,
    shield: 0,
    def: 0,
    dmg: 0,
    
    rep: 1, // max 10
    fate: 3,
    life:3,
    
    hp: 20,
    hp_adj: 0,
    
    hero_path: 'why not',
    race: 'gobelin',
    gp: 0,

    str_exp:0,
    dex_exp:1,
    int_exp:5,
    
};

export default default_adventurer;


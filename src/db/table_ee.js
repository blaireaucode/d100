/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import * as th from '../table_helpers.js';
import table_ea from './table_ea.js';

const table_ee = `
D100 Monster Type AV Def Dmg HP Loot Abilities
-39 ~ -37 Cave Cricket c 15 0 -3 4 [K:Nothing]
-36 ~ -34 Slime ud 20 1 -3 6 [K:Table I-20 x2/Oil x4] Split, Mindless
-33 ~ -31 Centipede c 20 -1 -1 5 [K:Table P] Poison
-30 ~ -25 Giant Rats c 25 0 -2 3/2/2 [K:Table P] Disease, Pack
-24 ~ -23 Trogolyte h 20 0 -2 5 [K:Table I/A]
-22 ~ -18 Bone Lizard ud 20 -1 -2 6 [K:Table P] Mindless
-17 ~ -13 Giant Bats c 25 0 -3 2/3/3 [K:Table P] Fly, Surprise, Pack
-12 ~ -11 Carnivorous Toad c 20 0 -4 10 [K:Table P] Poison
-10 ~ -6 Cultist h 30 0 -1 8 [K:Table I/A]
-5 ~ -2 Will-o-Wisp d 25 0 -1 9 [K:Table I-10/Oil x2] Fire
-1 ~ 0 Gastropod c 20 2 -3 7 [K:Table P] Freeze, Armoured
1 ~ 2 Hounds c 25 -1 0 2/2/3/3 [K:Table P] Stunned, Pack
3 ~ 6 Giant Ants c 20 1 -2 3/2/3/3 [K:Table P] Pack
7 ~ 8 Animated Armour ud 30 2 0 4 [K:Table A-20 x2] Tough, Armoured
9 ~ 10 Antlion c 35 0 0 9 [K:Table P] Surprise
11 ~ 12 Scorpion c 35 1 -2 7 [K:Table P] Poison, Surprise
13 ~ 16 Giant Spider c 30 2 -1 6 [K:Table P] Web
17 ~ 21 Gobalotes h 25 0 -2 3/3/2 [K:Table I/W] Pack
22 ~ 25 Goblins h 25 1 -1 2/3/3 [K:Table A/ I/W] Pack
26 ~ 28 Goblin Archers h 25 1 -1 2/3/3 [K:Table A/I/W] Pack, Surprise
29 Goblin Warlock h 30 1 -1 7 [K:Table I/TA] Dark Magic
30 ~ 31 Crab c 35 3 0 8 [K:Table P] Armoured
32 ~ 36 Ooze ud 30 4 -1 9 [K:Table I-10 x3 / Oil x6] Split, Mindless
37 ~ 38 Bear c 40 1 0 10 [K:Table P +5]
39 ~ 40 Crocodile c 45 2 +2 11 [K:Table P+5] Armoured
41 ~ 45 Vampire Leech ud 35 0 +1 10 [K:Table P+10] Regenerate, Mindless
46 ~ 47 Ratman h 30 2 0 3/3/4 [K:Table A/I/W] Pack, Disease
48 Ratman Champion h 40 2 0 9 [K:Table A/I/W] Disease
49 ~ 50 Goatman h 40 3 +1 11 [K:Table A/I/W]
51 ~ 52 Goatman Archer h 40 2 +1 9 [K:Table A/I/W] Surprise
53 Goatman Priest h 40 1 +1 15 [K:Table I/TA] Dark Magic
54 ~ 56 Zombies ud 35 0 0 4/4/4/5 [K:Nothing] Disease, Pack, Mindless
57 ~ 58 Tricksters d 35 1 0 3/4/4 [K:Table I/W +5] Pack, Surprise, Dark Magic
59 ~ 60 Imps d 35 1 +1 4/4/5/5 [K:Table P/I/W +5] Fire, Pack
61 ~ 63 Orc h 45 3 +1 10 [K:Table A/I/W +10]
64 ~ 66 Orc Archer h 45 2 +1 9 [K:Table A/I/W +10] Surprise
67 ~ 68 Golem h 40 4 -1 7 [K:Table S Scroll] Tough, Regenerate, Mindless
69 Spider Queen c 40 3 +2 14 [K:Table P +10] Web, Surprise, Poison
70 Mimic ud 25 5 +5 5 [K:Table TA-20/Ix3] Surprise
71 Skeleton Spiders ud 45 4 +2 15 [K:Table P +10] Regenerate, Resurrection, Web, Surprise
72 ~ 73 Harpy h 35 2 +3 17 [K:Table P+10/TB-50] Fly, Disease
74 Vampire Bat c 45 3 +1 10 [K:Table P +10] Fly, Surprise, Phase,Resurrection
75 ~ 76 Giant Apes c 40 2 +2 6/6/8 [K:Table P +10] Pack
77 ~ 78 Mummy ud 35 0 0 20 [K:Table S Scroll] Mindless, Disease, Poison
79 Zombie Master ud 50 1 +1 16 [K:Table TA +10] Disease
80 ~ 81 Skeleton ud 50 4 +1 13 [K:Table A/W +15] Fear, Regenerate, Resurrection, Mindless
82 Gorgon c 35 2 0 15 [K:Table P/I +5 x2] Attacks 2, Poison
83 Orc Champion h 55 5 +2 16 [K:Table A/I/W +15]
84 ~ 85 Giant Snake c 55 3 +2 16 [K:Table P +10] Poison
86 Ghoul ud 50 3 +3 18 [K:Table P +10] Phase, Fear, Mindless
87 Wight ud 55 4 +3 20 [K:Table I/TA] Fly, Fear, Resurrection
88 ~ 89 Cyclops h 50 2 +1 7/7/9 [K:Table W/A+5/TA-50] Large, Pack
90 Orc Warlock h 50 4 +2 22 [K:Table I/TA] Dark Magic
91 Demon d 55 4 +2 20 [K:Table P/I/W +15] Fire
92 ~ 93 Wyvern c 50 3 +1 23 [K:Table P/A+15/TA-20] Fly, Poison
94 Ghost ud 60 4 +2 24 [K:Table TA +15] Death Touch, Ethereal, Fear, Mindless
95 Vampire ud 65 5 +3 25 [K:Table I/W/TA +15] Dark Magic, Fly, Surprise, Phase,Resurrection
96 ~ 97 Basilisk c 50 4 +2 18 [K:Table P+10] Poison, Armoured, Stun
98 Wrath ud 60 4 +3 28 [K:Table TB] Death Touch, Ethereal, Fear
99 ~ 100 Hydra c 45 3 0 21 [K:Table P/W+20/TA-10] Armoured, Attacks 3, Regenerate
101 Necromancer ud 60 5 +2 25 [K:Table I/W +20/TB] Dark Magic, Regenerate, Resurrection
102 Demon Lord d 55 4 +2 29 [K:Table P/I/W +20/TB] Fire, Large, Fear, Fly
103 Beholder c 50 1 +4 35 [K:Table A/W/TA+20] Surprise, Web, Fly, Freeze
104 Ogre h 60 5 +3 30 [K:Table P+20/TB+5] Fear, Large
105 Minotaur c 65 6 +3 33 [K:Table P+20/TB+5] Fear, Large
106 Giant h 65 6 +4 35 [K:Table P+20/TB+10] Fear, Large, Stun
107 Troll c 60 5 +3 36 [K:Table P+20/TB+10] Large, Fear, Regenerate
108 Evil Warlock h 65 6 +3 34 [K:Table P+20/TB+15] Dark Magic
109 Liche Lord ud 70 7 +3 35 [K:Table I/W+20/TB+15] Dark Magic, Regenerate, Resurrection
110 Gargoryle d 65 7 +3 38 [K:Table P/TB+20] Fear, Stun, Tough
111 Wyrm c 70 6 +4 40 [K:Table P+20/TC] Fear, Attacks 2, Large
112 Fire Wyrm c 75 7 +4 42 [K:Table P+20/TC] Fire, Fear, Attacks 2, Large
113 Frost Wyrm c 75 7 +4 45 [K:Table P+20/TC] Freeze, Fear, Attacks 2, Large
114 White Dragon c 80 7 +5 47 [K:Table P+20/TC+5] Fly, Fear, Attacks 2, Large
115 Green Dragon c 80 8 +5 49 [K:Table P+20/TC+5] Poison, Fly, Fear, Attacks 2, Large
116 Red Dragon c 85 8 +5 44 [K:Table P+20/TC+10] Fire, Fly, Fear, Attacks 2, Large
117 Blue Dragon c 80 7 +5 45 [K:Table P+20/TC+10] Freeze, Fly, Fear, Attacks 2, Large
118 Black Dragon c 85 8 +5 47 [K:Table P+20/TC+15] Dark Magic, Fly, Fear, Attacks 2, Large
119 Skeletal Dragon c 90 9 +6 48 [K:Table P+20/TC+15] Ethereal, Fly, Fear, Attacks 2, Large
120 Golden Dragon c 90 9 +6 50 [K:Table P/TC+20] Tough, Stun, Fly, Fear, Attacks 2, Large, Fire

`;

// https://stackoverflow.com/questions/5034781/js-regex-to-split-by-line
//const lines = table_ee.split(/\r?\n/);    // match empty lines
const lines = table_ee.match(/[^\r\n]+/g);  // does not match empty lines
//export default lines;

// ----------------------------------------------------------------------------
function parse_encounter_line(line)
{
    var a = parseInt(line);
    var b = a;
    if (isNaN(a)) return "";
    
    line = th.s_rm(line, a);
    if (line[0] === '~') {
        line = th.s_rm(line, "~");
        b = parseInt(line);
        if (isNaN(b)) return "";
        line = th.s_rm(line, b);
    }
    const D100_1 = a;
    const D100_2 = b;     
    
    //D100 Monster Type AV Def Dmg HP Loot Abilities
    // console.log('dd', D100_1, D100_2);
    // console.log('dd', line);
    var words = line.split(" ");
    var index = 1;

    // Monster name
    var Monster = words[0];
    var w = words[index];
    while (w !== 'c' &&
           w !== 'ud' &&
           w !== 'd' &&
           w !== 'h') {
        Monster = Monster+' '+w;
        index = index+1;
        w = words[index];
    }
    Monster = Monster.trim();

    // Type
    const Type = words[index].trim();
    index = index+1;

    // AV
    const AV = parseInt(words[index]);
    index = index+1;

    // Def
    const Def = parseInt(words[index]);
    index = index+1;

    // Def
    const Dmg = parseInt(words[index]);
    index = index+1;

    // HP
    const HP = words[index].trim();
    index = index+1;

    // Loot
    var Loot = '';//words[0];
    w = words[index];
    while (w[w.length-1] !== ']') {
        Loot = Loot+' '+w;
        index = index+1;
        w = words[index];
    }
    Loot = Loot+' '+w;
    index = index+1;
    Loot = Loot.trim();

    // FIXME <------------ parse Loot

    // Abilities
    var Abilities = '';
    while (index<words.length) {
        Abilities = Abilities+words[index];
        index = index+1;
    }
    Abilities = Abilities.trim();
    var awords = Abilities.split(",");
    var ab = [];
    for (var v of awords) {
        // console.log('v',v);
        ab.push({name:v, help:table_ea[v]});
        // console.log('table',table_ea[v]);
    }
    
    // return
    return { D100_1:D100_1,
             D100_2:D100_2,
             name:Monster,
             type:Type,
             av:AV,
             def:Def,
             dmg:Dmg,
             hp:HP,
             loot:Loot,
             abilities:ab};
}
// ----------------------------------------------------------------------------


// Create the structure from the table
var ee = [];
var l = [];
for(var line of lines) {
    // console.log('line', line);
    const e = parse_encounter_line(line);    
    // console.log('e', e);
    if (e !== '') ee.push(e);
    l.push(line.trim());
}
// console.log('ee',ee);

const h = 'D100 D100 Monster Type AV Def Dmg HP Loot Abilities';

export default {
    key:'encounters',
    title: 'Encounters',
    header:h,
    width:th.get_max_width(lines),
    rows:l,
    fields:ee
};




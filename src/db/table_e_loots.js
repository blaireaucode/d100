/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

const table_e_loots = [];

table_e_loots['Attacks'] = `The Monster is able to attack more than
once in a single round of combat, and the number of times it gets to
make an attack roll is equal to its Attacks rating. For each attack
that scores a hit, roll a damage die and add them together for a total
damage score, then roll for a hit location and apply its modifier (if
any).  Finally apply the Monsters Dmg modifier (if any).`;

table_e_loots['Dark magic'] = ` Spell Casters found in the dungeons
have attuned themselves with Dark Magic, so at the start of each
Combat Round before step 1, roll 1d10 to see which magic spell they
will use for the round.

1d10 Spell Description
1-2 Dark Strike: If the Monster attacks this combat round it gains +20 AV.
3-4 Shadowy Cloak For this combat round the Monsters gains +4 DEF.
5-6 Evil Touch For this combat round the Monsters gains +2 DMG.
7-8 Drain Life For this combat round each HP the Adventurer loses restores an equal
number to the Monster.
9-0 Death Bolt The Adventurer loses 2 HP.`;


export default table_e_loots;


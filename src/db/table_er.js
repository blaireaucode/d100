/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import * as th from '../table_helpers.js';

const table_ee = `
10 Monster will Escape
9 Monster damaged last Round it will Escape
8 Monster has less Half HP will Escape
7 Monster will Attack as normal
6 Monster will Attack as normal
5 Monster will Attack as normal
4 Monster will Attack as normal
3 Monster will Attack as normal
2 Monster has less than 1/2 its HP gains AV+5
1 Monster has less than 1/2 its HP gains AV+10
`;

// https://stackoverflow.com/questions/5034781/js-regex-to-split-by-line
//const lines = table_ee.split(/\r?\n/);    // match empty lines
const lines = table_ee.match(/[^\r\n]+/g);  // does not match empty lines

const h = 'D10 Monsters Reaction';

export default {
    key:'enc_reaction',
    title: 'Monster Reaction',
    header:h,
    width:th.get_max_width(lines),
    rows:lines};


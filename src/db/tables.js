/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import table_er from './table_er.js'; // encounter reaction
import table_ee from './table_ee.js'; // encounter extended
import table_ea from './table_ea.js'; // encounter abilities
import table_hl from './table_hl.js'; // hit location

import table_d from './table_d.js'; // Door
import table_f from './table_f.js'; // Finding
import table_g from './table_g.js'; // Geographic
import table_w from './table_w.js'; // Weapons
import table_a from './table_a.js'; // Armour
// import table_m from './table_hl.js'; // Mapping

var tables = [];

tables[table_er.key] = table_er;
tables[table_hl.key] = table_hl;
tables[table_ee.key] = table_ee;
tables[table_ea.key] = table_ea;
tables[table_d.key] = table_d;
tables[table_g.key] = table_g;
tables[table_f.key] = table_f;
tables[table_w.key] = table_w;
tables[table_a.key] = table_a;

export default tables;

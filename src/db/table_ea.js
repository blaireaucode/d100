/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import * as th from '../table_helpers.js';
const e = `

# Attacks (X) #

The Monster is able to attack more than once in a single round of
combat, and the number of times it gets to make an attack roll is
equal to its Attacks rating. For each attack that scores a hit, roll a
damage die and add them together for a total damage score, then roll
for a hit location and apply its modifier (if any).  Finally apply the
Monsters Dmg modifier (if any). In short : Roll X times during Combat
Step 4. Roll each hit for Damage.

# Large #

The Monster is exceptionally large, and will often deal more damage
when it hits an Adventurer than smaller Monsters, therefore, damage
die results of 1 or 2 gain +2 dmg.

# Dark magic #

Spell Casters found in the dungeons have attuned themselves with Dark
Magic, so at the start of each Combat Round before step 1, roll 1d10
to see which magic spell they will use for the round.

1D10 Spell Description
1-2 Dark Strike: If the Monster attacks this combat round it gains +20 AV.
3-4 Shadowy Cloak For this combat round the Monsters gains +4 DEF.
5-6 Evil Touch For this combat round the Monsters gains +2 DMG.
7-8 Drain Life For this combat round each HP the Adventurer loses restores an equal
number to the Monster.
9-0 Death Bolt The Adventurer loses 2 HP.

In short: Roll 1d10 at start of each combat round. 1-2: Monster +20 AV ; 3-4: Monster +4 DEF ; 5-6: Monster +2 DMG ; 7-8: Monster heals 1 HP for each damage dealt ; 9-10: Character -2 HP.


# DEATH TOUCH #

Whilst encountering a Monster with Death Touch, the Adventurer suffers
-2 DEF and all equipped Armour that it hits has a rating of A0. Damage
may still be deflected as normal. (Character has -2 DEF).

# DISEASE #

When a Monster scores a natural 1 on their damage dice, they infect
the Adventurer with Disease and the player shades in one pip on the
Disease track of the Adventure Sheet, even if the Monster does not
deal any damage during the combat round. When the time track is
refreshed, the player must roll 1d10, and if the result is equal to or
less than the number of Disease Pips shaded, the Adventurer suffers HP
equal to the number of pips shaded. In short: If Monster rolls 1 on
Damage Die, shade 1 Disease Pip.

# ETHEREAL #

The Monster has no substance, and normal weapons will simply pass
through them. All attacks made against an ethereal Monster must be
with a legendary weapon or a form of spell (scroll) in order to
inflict any kind of damage. In short: can only be damaged by Spells,
Scrolls & Legendary Weapons.

# FEAR #

The Monster may strike fear in any Adventurer that it comes face to
face with, and at the start of each combat round the Adventurer must
make a Fear test. FEAR Test: Int -10 [S: Attack as Normal] [F: Forfeit
next Attack, Attack Action] (Bravery).

# FIRE #

The Monster, in addition to its normal attack, has some sort
of fire attack that may strike an Adventurer. Each time a natural 1 is
rolled for its damage, the Adventurer may be set on fire by its attack
and must make an Avoid Fire Test. In short: if Monster rolls 1 on
Damage Die, Test Avoid Fire. AVOID FIRE Test: Dex -5 [S: No Effect]
[F:-2HP] (Dodge).

# FLY #

An Adventurer fighting a flying Monster suffers -10 STR when making an
attack roll, but if using a ranged weapon (Dex) it has no effect. In
short: character has -10 STR if attacking with a Melee weapon.

# FREEZE #

The Monster, in addition to its normal attack, has some sort of freeze
attack that may strike an Adventurer, and each time a natural 1 is
rolled for its damage, the Adventurer may be frozen by its attack and
must make an Avoid Freeze Test. If Monster rolls 1 on Damage Die, Test
Avoid Freeze.  AVOID FREEZE Test: Dex [S: No Effect] [F: -1HP, -2 DEF
in next combat round] (Dodge).

# LARGE #

The Monster is exceptionally large, and will often deal more damage
when it hits an Adventurer than smaller Monsters, therefore, damage
die results of 1 or 2 gain +2 dmg. In short: reroll all Monster's
damage dice results of 1 & 2.

# PACK #

At the start of each combat round, each Monster still alive beyond the
first adds +5 to its AV. For instance, if an Encounter has 4 Monsters
still alive, they get AV+15 when they attack. When an Adventurer deals
a pack damage, its HP is depleted in the order from left to right, and
any surplus damage spills over to the next and so on.  For instance,
if an Adventurer faces a pack with HP 3/2/2 and has just dealt 4
damage, the left most pack Monster is defeated and the second receives
1 point of damage and will have the following HP remaining 0/1/2. In
short: combat Step 1 - Add +5 AV for each packmate alive beyond the
first.


# PHASE #

The Monster can phase in and out of reality, making it a hard target
to strike. The Adventurer suffers -10 to their Str or Dex whilst
attacking a Monster with the Phase Ability. In short: character has
-10 STR and -1 DEX when attacking this Monster.

# POISON #

When a Monster scores a natural 1 on their damage die, they infect the
Adventurer with Poison and the player shades in one pip on the Poison
track of the Adventure Sheet, even if the Monster does not deal any
damage during the combat round. When the time track is refreshed, the
player must roll 1d10, and if the result is equal to or less than the
number of Poison Pips shaded, the Adventurer suffers HP equal to the
number of shaded pips, and the player removes 1 pip from the Poison
Track. In short: if Monster rolls 1 on Damage Die, shade 1 Poison Pip

# RESURRECTION #

When the Monster is killed, it may resurrect and come back to
life. After killing a Monster with Resurrection and before it has been
looted, roll 1d10. If a 1 is rolled it immediately returns to life
with full HP, and the Adventurer must once again attempt to kill
it. In short: if Monster's HP <= 0, roll 1d10. On 1, restore to full
HP.

# REGENERATE # The Monster has the ability to restore lost HP. Each time it roll a natural 1 on the damage die it will restore 2 lost HP. In short: If Monster rolls 1 on Damage Die, heal 1HP instead of attacking.

bidon bidon        bidon

# STUN #

The Monsters attack may cause an Adventurer to become stunned, and
each time the Monster deals damage to the Adventurer, they must make a
Dazed test. In short: when Monster deals damage, Test Dazed. DAZED
Test: STR-15 [S: No Effect] [F: Forfeit next Attack, Attack Action,
Escape] (Dodge).

# SURPRISE #

The Monster may surprise the Adventurer when it first appears, before
the first round of combat begins make a Surprise test. In short:
before 1st combat round begins, Test Surprise.  SURPRISE Test Int-10
[S: No Effect] [F: Monster Free Attack] (Aware)

# WEB #

At the end of each combat round in which the Monster is still alive,
the Adven turer makes an Avoid Web Test to determine if they will get
an Attack, or make an Attack Action next Combat Round. In short: zt
the end of each Combat Round, Test Avoid Web. AVOID WEB Test: Dex +20
[S: Attack as Normal] [F: Forfeit Attack, Attack Action, Escape]
(Dodge)

# Armoured #

Character has -10 DEX if attacking with a Ranged weapon Monster heals
1 HP for each damage dealt ; 9-10: Character -2 HP


# Mindless #

Ignore any Monster Reactions that say Escape

# Split #

When Monster's HP <= half, Attacks 2

# DAZED #

Test: STR-15 [S: No Effect] [F: Forfeit next attack, No Escape, cannot
use belt slots] (Dodge).

# Tough #

Each Monster HP requires 2 points of damage to remove.

`;


//const lines = e.match(/[^\r\n]+/g);  // does not match empty lines

const words = e.split('#');

var f = false;
var name = '';
var table_ea = [];
var rows = [];
for(var w of words) {
    if (f) {
        name = w.trim().toLowerCase();
        name = name.charAt(0).toUpperCase() + name.slice(1);
        f = false;
    }
    else {
        table_ea[name] = w;
        f = true;
        if (name)
            rows.push('* '+name+'. '+w);
    }
}

const h = 'name help';

export default {
    key:'encounter_abilities',
    title: 'Encounter abilities',
    header:h,
    width:th.get_max_width(rows),
    rows:rows,
    // fields:ee
};


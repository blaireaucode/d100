/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import * as th from '../table_helpers.js';

const table_hl = `
1 Head +3
2 Back +2
3 Torso +1
4 Arms 0
5 Hands 0
6 Main Weapon 0
7 Off Weapon 0
8 Waist Belt Check
9 Legs -1
10 Feet -1
`;

// https://stackoverflow.com/questions/5034781/js-regex-to-split-by-line
//const lines = table_ee.split(/\r?\n/);    // match empty lines
const lines = table_hl.match(/[^\r\n]+/g);  // does not match empty lines

const h = 'D10 Hit Location Damage Mod';

var hl = [];
for(var line of lines) {
    const words = line.split(' ');
    const d = words[0];
    const txt = line; // FIXME
    hl[d] = txt;
}


export default {
    key:'hit_location',
    title: 'Hit Location',
    header:h,
    width:th.get_max_width(lines),
    rows:lines,
    fields:hl
};


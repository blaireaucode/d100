/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import * as th from '../table_helpers.js';

// ----------------------------------------------------------------------------
function parse_door_line(line)
{
    // D100
    var a = parseInt(line);
    var b = a;
    if (isNaN(a)) return "";
     line = th.s_rm(line, a);
    if (line[0] === '-') {
        line = th.s_rm(line, "-");
        b = parseInt(line);
        if (isNaN(b)) return "";
        line = th.s_rm(line, b);
    }
    const D100_1 = a;
    const D100_2 = b;
    line = line.trim();
    line = th.s_rm(line, ',');
    line = line.trim();
    // console.log('D100', D100_1, D100_2, line);

    // Time
    var words = line.split(" ");
    var index = 0;
    var time = '';
    var w = words[index];
    while (w !== 'clock') {
        time = time+' '+w;
        index+=1;
        w = words[index];
    }
    time = time+' '+w;
    line = th.s_rm(line, time);
    // console.log('time', time);

    // Details
    const details = line;
    // console.log('details', details);

    return {D100_1:D100_1,
            D100_2:D100_2,
            time:time,
            details:details
    };
}

// ----------------------------------------------------------------------------
const h = 'D100 D100 Time Details';

const table_hl = `
1-5, +2 clock As you move away some junk you hear a click and wonder if you have triggered a trap, roll 1d10 on TABLE G
6-10, +2 clock With your backed turned a Monster jumps out from its hiding, roll on TABLE E - ENCOUNTER and fight, it has SURPRISE.
11-15, +2 clock Moving through some of the rubbish strewn around the floor a small snake lashes out and bites your hand injecting its venom into a vein, shade in two pip of your Poison track.
16-20, +1 clock Searching through some of the larger heaps of debris you abruptly stop as you the pile is made up of diseased and rotting corpses, shade in 2 pips of your Disease Track.
21-25, +1 clock While searching around a piece of your equipment gets caught in a crevices of the dungeon wall, carefully you prise it free but it has suffered some minor damage, roll one d10 for location of the item and shade in 1 pip of its repair track, if no item is equipped roll again until you select an item that is equipped.
26-30, +1 clock You accidentally stumble and fall landing on your side, make a belt check.
31-35, +1 clock You find nothing of any interest.
36-40, +1 clock After a lot of digging through little more than junk you do find something of value, roll on TABLE I - ITEMS.
41-45, +1 clock This area is dank and foul smelling, which is no surprise when you come across a Tomb hidden behind some broken furniture. If you wish you can spend some time opening it, +1  clock on the time track. 1 = It is empty. 2-4 = You find a corpse clutching a bag of gold (+480gp). 5-10 = Inside apart from some old bones is a Treasure, roll on TABLE TA - TREASURE A -15.
46-50, +1 clock You have found a magic scroll,  roll x1 on TABLE S - SPELLS to determine which spell it contains.
51-55, +1 clock Something catches your eye and you find something useful, roll on TABLE R - REQUIRED to find out what it is.
56-60, +1 clock Amidst some rubble you find a weapon, roll x1 on TABLE W - WEAPONS+15 to find out which weapon it is.
61-65, +1 clock Searching through some junk you find a torn page from a spell book, roll x1 on TABLE S - SPELLS to determine which spell.
66-70, +1 clock On one of the walls behind a badly hung curtain you find a small lever, when you pull it you hear a rumble from somewhere in the dungeon (shade in 1 Lever Pip)
71-75, +0 clock You clear some debris to find a piece of armour, roll on TABLE A - ARMOUR+15 to find out which armour it is.
76-80, +0 clock You find a SECRET tunnel - make a thin exit through the rock face of your current areas to line up with a middle section of a mapped or unmapped adjacent area. (====S====)
81-85, +0 clock You have found a book of spells amongst some old books, roll x2 on TABLE S - SPELLS to determine which spell it contains.
86-90, +0 clock Well hidden behind a broken cupboard you find a hole in the dungeon wall, inside is a treasure, roll on TABLE TA - TREASURE A, apply -15 to your roll.
91-95, +0 clock You come across a small worn carpet that appears out of place for this area, moving it to one side reveals a wooden panel flush with the dungeon floor that once would have been locked, now it is broken beyond repair, you life the panel out and to your surprise find a treasure laying in a rough hollow, roll on TABLE TB - TREASURE B, apply -15 to your roll.
96-100, +0 clock Shifting through the rubbish strewn about the floor, you are startled to find an entire skeleton, once it must have been a brave adventurer like yourself, now it has been a feed for the small insects and rats that inhabit the dungeon, a quick search reveals the poor chap had very little at the time of his death, that is all apart from this magnificent treasure, roll on TABLE TC - TREASURE C, apply -15 to your roll.

`;

// https://stackoverflow.com/questions/5034781/js-regex-to-split-by-line
//const lines = table_ee.split(/\r?\n/);    // match empty lines
const lines = table_hl.match(/[^\r\n]+/g);  // does not match empty lines

var rows = [];
var fields = [];
for(var line of lines) {
    // console.log('dline', line);
    const d = parse_door_line(line);
    if (d !== '') fields.push(d);
    rows.push(line.trim());
}


export default {
    key:'find',
    title: 'Find',
    header:h,
    width:th.get_max_width(lines),
    rows:rows,
    fields:fields
};

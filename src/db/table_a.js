/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import * as th from '../table_helpers.js';

// ----------------------------------------------------------------------------
function parse_armour_line(line)
{
    // D100
    var a = parseInt(line);
    var b = a;
    if (isNaN(a)) return "";
     line = th.s_rm(line, a);
    if (line[0] === '-') {
        line = th.s_rm(line, "-");
        b = parseInt(line);
        if (isNaN(b)) return "";
        line = th.s_rm(line, b);
    }
    const D100_1 = a;
    const D100_2 = b;
    line = line.trim();
    line = th.s_rm(line, ',');
    line = line.trim();
    // console.log('D100', D100_1, D100_2, line);

    const words = line.split("|");
    var index=0;

    const slot = words[index].trim();
    index+=1;

    const name = words[index].trim();
    index+=1;

    const AS = words[index].trim();
    index+=1;

    const value = parseInt(words[index], 10);
    index+=1;

    const fix = parseInt(words[index], 10);
    index+=1;

    return {D100_1:D100_1,
            D100_2:D100_2,
            item_type:'armour',
            armour:true,
            slot:slot,
            name:name,
            AS:AS,
            value:value,
            fix:fix
    };
}

// ----------------------------------------------------------------------------
const h = 'D100 D100 Slot Name AS Value Fix';

const table_raw = `
1-4 | Feet | LEATHER BOOTS | A0  |  67 | 14|
5-8 | Legs | LEATHER TASSETS | A0  |  68 | 14|
9-12 | Waist | LEATHER GIRDLE | A0  |  70 | 14|
13-16 | Off Hand | BUCKLER SHIELD | S0  |  79 | 16|
17-20 | Hands | LEATHER GAUNTLETS | A0  |  73| 15|
21-24 | Arms | LEATHER ARMS GUARDS | A0  |  66 | 14|
25-28 | Torso | LEATHER CUIRASS | A0  |  78 | 16|
29-32 | Back | LEATHER CLOAK | A0  |  67 | 14|
33-36 | Head | LEATHER CAP | A0  |  75 | 15|
37-39 | Feet | STUDDED LEATHER SOLLERETS | A1  |  87 | 18|
40-42 | Legs | STUDDED LEATHER CHAUSSES | A1  |  89 | 18|
43-45 | Waist | STUDDED LEATHER BELT | A1  |  94 | 19|
46-48 | Off Hand | TARGE SHIELD | S1  |  123 | 27|
49-51 | Hands | STUDDED LEATHER GLOVES | A1  |  97 | 20|
52-54 | Arms | STUDDED LEATHER BRACERS | A1  |  83 | 17|
55-56 | Torso | STUDDED LEATHER BRIGANDINE | A1  |  123 | 25|
57-58 | Back | STUDDED LEATHER CAPE | A1  |  87 | 18|
59-60 | Head | STUDDED LEATHER HELMET | A1  |  104 | 21|
61-62 | Feet | MAIL SABATONS | A2  |  143 | 29|
63-64 | Legs | MAIL CUISSE | A2  |  146 | 30|


39-41 | Feet |  | A1  |  31gp | 7gp|
55-57 | Hands | MAIL HANDWRAPS | Defense=2 DEF  |  47gp | 10gp|
58-60 | Torso | MAIL SHIRT | Defense=2 DEF  |  58gp | 12gp|
61-63 | Head | MAIL COIF | Defense=2 DEF  |  44gp | 9gp|
64-66 | Waist | PADDED MAIL BELT | Defense=2 DEF  |  41gp | 9gp|
70-72 | Off Weap. | HEATER SHIELD | Defense=3  |  83gp | 17gp|
73-75 | Arms | MAIL SLEEVES | Defense=2 DEF  |  49gp | 10gp|
76-77 | Torso | SCALE MAIL HAUBERK | Defense=3 DEF  |  60gp | 12gp|
78-79 | Waist | SCALE MAIL FAULD | Defense=3 DEF  |  55gp | 11gp|
80-81 | Feet | SCALE MAIL BOOT | Defense=3 DEF  |  51gp | 11gp|
82-83 | Legs | SCALE MAIL POLEYN | Defense=3 DEF  |  58gp | 12gp|
84-85 | Arms | SCALE MAIL VAMBRACE | Defense=3 DEF  |  56gp | 12gp|
86-87 | Off Weap. | KITE SHIELD | Defense=4  |  97gp | 20gp|
88-89 | Hands | SCALE MAIL GLOVES | Defense=3 DEF  |  54gp | 11gp|
90-91 | Head | SCALE MAIL ARMET | Defense=3 DEF  |  52gp | 11gp|
92 | Hands | PLATE MAIL MANIFERS | Defense=4 DEF  |  68gp | 14gp|
93 | Arms | PLATE MAIL BRACERS | Defense=4 DEF  |  66gp | 14gp|
94 | Torso | PLATE MAIL BREASTPLATE | Defense=4 DEF  |  79gp | 16gp|
95 | Legs | PLATE MAIL GREAVES | Defense=4 DEF  |  63gp | 13gp|
96 | Off Weap. | PAVISE SHIELD | Defense=5  |  104gp | 21gp|
97 | Feet | PLATE MAIL SABATONS | Defense=4 DEF  |  63gp | 13gp|
98 | Head | PLATE MAIL GREAT HELM | Defense=4 DEF  |  67gp | 14gp|
99 | Waist | PLATE MAIL GIRDLE | Defense=4 DEF  |  66gp | 14gp|
100 | Back | LEGENDARY CLOAK Roll on Table L for its Legend. | A0  |  +500 | +100|

`;


// https://stackoverflow.com/questions/5034781/js-regex-to-split-by-line
//const lines = table_ee.split(/\r?\n/);    // match empty lines
const lines = table_raw.match(/[^\r\n]+/g);  // does not match empty lines

var rows = [];
var fields = [];
for(var line of lines) {
    // console.log('dline', line);
    const d = parse_armour_line(line);
    if (d !== '') fields.push(d);
    //console.log(d);
    rows.push(line.trim());
}


export default {
    key:'armours',
    title: 'Armours',
    header:h,
    width:th.get_max_width(lines),
    rows:rows,
    fields:fields
};

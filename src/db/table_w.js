/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import * as th from '../table_helpers.js';

// ----------------------------------------------------------------------------
function parse_weapon_line(line)
{
    // D100
    var a = parseInt(line);
    var b = a;
    if (isNaN(a)) return "";
     line = th.s_rm(line, a);
    if (line[0] === '-') {
        line = th.s_rm(line, "-");
        b = parseInt(line);
        if (isNaN(b)) return "";
        line = th.s_rm(line, b);
    }
    const D100_1 = a;
    const D100_2 = b;
    line = line.trim();
    line = th.s_rm(line, ',');
    line = line.trim();
    // console.log('D100', D100_1, D100_2, line);

    const words = line.split("|");
    var index=0;

    var type = words[index].trim();
    if (type === 'x') type = 1;
    else type = 2;
    index+=1;

    const handed = words[index].trim();
    index+=1;

    const name = words[index].trim();
    index+=1;

    const adj = parseInt(words[index], 10);
    index+=1;

    const value = parseInt(words[index], 10);
    index+=1;

    const fix = parseInt(words[index], 10);
    index+=1;

    return {D100_1:D100_1,
            D100_2:D100_2,
            weapon:true,
            item_type:'weapon',
            slot:type,
            type:handed,
            name:name,
            dmg:adj,
            gp:value,
            fix:fix
    };
}


// ----------------------------------------------------------------------------
const h = 'D100 D100 slot Type Name Adj Value Fix';

const table_raw = `
1-2 | x | R | SLING | -2 |  12 | 3|
3-4 | xx | R | CATAPULT | -2 |  15 | 3|
5-6 | x | H | BATON | -2 |  23 | 5|
7-8 | xx | H | STAVE | -2 |  32 | 7|
9-10 | x | H | KNIFE | -2 |  44 | 9|
11-12 | x | H | CLUB | -1 |  50 | 10|
13-14 | xx | H | QUARTERSTAFF | -1 |  51 | 11|
15-16 | x | H,R | THROWING KNIFE | -1 |  54 | 11|
17-18 | x | H | DAGGER | -1 |  54 | 11|
19-20 | x | H | SPIKED CLUB | +0 |  57 | 12|
21-22 | x | H | HAMMER | +0 |  60 | 12|
23-24 | x | H,R | THROWING SPEAR | +0 |  66 | 14|
25-26 | xx | R | SHORT BOW | +0 |  67 | 14|
27-28 | x | H | WAR PICK | +0 |  68 | 14|
29-30 | x | H | SHORT SWORD | +0 |  70 | 14|
31-32 | x | H | SCIMITAR | +0 |  73 | 15|
33-34 | x | H | RAPIER | +0 |  75 | 15|
35-36 | x | H | SCYTHE | +0 |  75 | 15|
37-38 | x | H | MACE | +0 |  78 | 16|
39-40 | x | H,R | THROWING AXE | +0 |  87 | 18|
41-42 | x | H,R | CHAKRAM | +0 |  89 | 18|
43-44 | x | R | REPEATING CROSSBOW | +0 |  94 | 19|
45-46 | xx | H | LANCE | +1 |  123 | 25|
47-48 | xx | H,R | SPEAR | +1 |  132 | 27|
49-50 | xx | H | MAUL | +1 |  134 | 27|
51-52 | x | H | FALCHION | +1DMG  |  143 | 29|
53-54 | xx | R | BOW | +1 |  146 | 30|
55-56 | x | H | AXE | +1 |  165 | 33|
57-58 | x | H | MORNING STAR | +1 |  167 | 34|
59-60 | x | H | BROADSWORD | +1 |  178 | 36|
61-62 | x | H | PERNACH | +1 |  178 | 36|
63-64 | xx | H | MILITARY FORK | +1 |  187 | 38|
65-66 | xx | H | PARTISAN | +1 |  189 | 38|
67-68 | xx | H | GLAIVE | +1 |  190 | 38|
69-70 | xx | H | HALBERD | +2 |  234 | 47|
71-72 | xx | H | BILL | +2 |  236 | 48|
73-74 | xx | H | TWO HANDED FLAIL | +2 |  243 | 49|
75-76 | xx | R | RECURVE BOW | +2 |  256 | 52|
77-78 | x | H | HALF MAUL | +2 |  265 | 53|
79-80 | xx | H | WAR SCYTHE | +2 |  266 | 54|
81-82 | xx | H | BARDICHE | +2 |  267 | 54|
83-84 | x | H | LONG SWORD | +2 |  278 | 56|
85-86 | xx | H | BATTLE AXE | +2 |  287 | 58|
87-88 | x | H | CLAYMORE | +2 |  290 | 58|
89-90 | xx | R | CROSSBOW | +2 |  298 | 60|
91-92 | xx | H | WAR HAMMER | +3 |  367 | 74|
93-94 | xx | R | ARBALEST | +3 |  367 | 74|
95-96 | xx | R | LONG BOW | +3 |  378 | 76|
97-98 | xx | H | BASTARD SWORD | +3 |  378 | 76|
99 | xx | H | GREAT SWORD | +4 |  420 | 84|
100 | xx | H | MIGHTY CLAYMORE | +4 |  467 | 94|
`;



// https://stackoverflow.com/questions/5034781/js-regex-to-split-by-line
//const lines = table_ee.split(/\r?\n/);    // match empty lines
const lines = table_raw.match(/[^\r\n]+/g);  // does not match empty lines

var rows = [];
var fields = [];
for(var line of lines) {
    // console.log('dline', line);
    const d = parse_weapon_line(line);
    if (d !== '') fields.push(d);
    //console.log(d);
    rows.push(line.trim());
}


export default {
    key:'weapons',
    title: 'Weapons',
    header:h,
    width:th.get_max_width(lines),
    rows:rows,
    fields:fields
};

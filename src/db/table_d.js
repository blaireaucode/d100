/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import * as th from '../table_helpers.js';

// ----------------------------------------------------------------------------
function parse_door_line(line)
{
    // D100
    var a = parseInt(line);
    var b = a;
    if (isNaN(a)) return "";
     line = th.s_rm(line, a);
    if (line[0] === '-') {
        line = th.s_rm(line, "-");
        b = parseInt(line);
        if (isNaN(b)) return "";
        line = th.s_rm(line, b);
    }
    const D100_1 = a;
    const D100_2 = b;
    line = line.trim();
    line = th.s_rm(line, ',');
    line = line.trim();
    // console.log('D100', D100_1, D100_2, line);

    // Type    
    var words = line.split(" ");
    var index = 0;
    var type = '';
    var w = words[index];
    while (w[w.length-1] !== ')') {
        type = type+' '+w;
        index+=1;
        w = words[index];
    }
    type = type+' '+w;
    line = th.s_rm(line, type);
    // console.log('type', type);

    // Details
    const details = line;
    
    return {D100_1:D100_1,
            D100_2:D100_2,
            type:type,
            details:details
    };
}

// ----------------------------------------------------------------------------
const h = 'D100 D100 Type Details';

const table_hl = `
1-29, OPEN (OPEN) When the handle is turned, the door is open
30-31, LOCKED (LD) Roll equal to or less than the number of keys you have to open, if successful delete 1 key; or pick the lock LOCKED DOOR - Test: Dex (S: Open) (F:-1 Pick, +1  clock) (Locks)
32-33, TRAP LOCKED (TLD) To open test TRAP LOCKED DOOR - Test: Dex (S:Open) (F:-1 Pick, -1 hp, +1 clock) (Locks,Traps)
34-35, JAMMED (JD) To open test JAMMED DOOR - Test: Str (S: Open) (F:-1HP,+1 clock) (Strong)
36-37, LEVER (LVD) Roll equal to or less than the number of Levers you have to open, (S: Opens, Remove 1 lever pip) (F: Closed/To open test JAMMED DOOR - Test: Str (S: Open) (F:-1HP,+1 clock) (Strong)
38-39, TRAPPED (TD) To open test TRAPPED DOOR - Test: Dex (S: Open) (F:-1HP) (Traps)
40-41, MAGIC SEALED (MS) Open with an Open (Spell, Scroll)
42-43, OPEN (OD) The door is open
44-45, MAGIC SEALED (MS) Open with an Open (Spell, Scroll)
46-47, LOCKED (LD) Roll equal to or less than the number of keys you have to open, if successful delete 1 key; or pick the lock LOCKED DOOR - Test: Dex -5 (S: Open) (F:-1 Pick, +1 clock) (Locks)
48-49, JAMMED (JD) To open test JAMMED DOOR - Test: Str -5 (S: Open) (F:-1HP,+1 clock) (Strong)
50-51, LEVER (LVD) Roll equal to or less than the number of Levers you have to open, (S: Opens,Remove 1 lever pip) (F: Closed/To open test JAMMED DOOR - Test: Str -5 (S: Open) (F:-1HP,+1 clock) (Strong)
52-53, TRAPPED (TD) To open test TRAPPED DOOR - Test: Dex -5 (S: Open) (F:-2HP) (Traps)
54-55, TRAP LOCKED (TLD) To open test TRAP LOCKED DOOR - Test: Dex -5 (S:Open) (F:-1 Pick, -1 hp,+1 clock) (Locks,Traps)
56-57, OPEN (OPEN) The door is open
58-59, LOCKED (LD) Roll equal to or less than the number of keys you have to open, if successful delete 1 key; or pick the lock LOCKED DOOR - Test: Dex -10 (S: Open) (F:-1 Pick, +1 clock) (Locks)
60-61, TRAPPED (TD) To open test TRAPPED DOOR - Test: Dex -10 (S: Open) (F:-3HP) (Traps)
62-63, LEVER (LVD) Roll equal to or less than the number of Levers you have to open, (S: Opens, Remove 1 lever pip) (F: Closed/To open test JAMMED DOOR - Test: Str -10 (S: Open) (F:-1HP,+1 clock) (Strong)
64-65, TRAP LOCKED (TLD) To open test TRAP LOCKED DOOR - Test: Dex -10 (S:Open) (F:-1 Pick, -1 hp, +1 clock) (Locks,Traps)
66-67, MAGIC SEALED (MS) Open with an Open (Spell, Scroll)
68-69, JAMMED (JD) To open test JAMMED DOOR - Test: Str -10 (S: Open) (F:-1HP,+1 clock) (Strong)
70-71, OPEN (OPEN) The door is open
72-73, JAMMED (JD) To open test JAMMED DOOR - Test: Str -15 (S: Open) (F:-1HP,+1 clock) (Strong)
74-75, LOCKED (LD) Roll equal to or less than the number of keys you have to open, if successful delete 1 key; or pick the lock LOCKED DOOR - Test: Dex -15 (S: Open) (F:-1 Pick, +1 clock) (Locks)
76-77, MAGIC SEALED (MS) Open with an Open (Spell, Scroll)
78-79, TRAPPED (TD) To open test TRAPPED DOOR - Test: Dex -15 (S: Open) (F:-3HP) (Traps)
80-81, TRAP LOCKED (TLD) To open test TRAP LOCKED DOOR - Test: Dex -15 (S:Open) (F:-1 Pick, -1 hp, +1 clock) (Locks,Traps)
82-83, OPEN (OPEN) The door is open
84-85, LEVER (LVD) Roll equal to or less than the number of Levers you have to open, (S: Opens, Remove 1 lever pip) (F: Closed/To open test JAMMED DOOR - Test: Str -15 (S: Open) (F:-1HP,+1 clock) (Strong)
86-87, OPEN (OPEN) The door is open
88-89, MAGIC SEALED (MS) Open with an Open (Spell, Scroll)
90-91, LOCKED (LD) Roll equal to or less than the number of keys you have to open, if successful delete 1 key; or pick the lock LOCKED DOOR - Test: Dex -20 (S: Open) (F:-1 Pick, +1  clock) (Locks)
92-93, JAMMED (JD) To open test JAMMED DOOR - Test: Str -20 (S: Open) (F:-1HP,+1 clock) (Strong)
94-95, LEVER (LVD) Roll equal to or less than the number of Levers you have to open, (S: Opens, Remove 1 lever pip) (F: Closed/To open test JAMMED DOOR - Test: Str -20 (S: Open) (F:-1HP,+1 clock) (Strong)
96-97, TRAPPED (TD) To open test TRAPPED DOOR - Test: Dex -20 (S: Open) (F:-4HP) (Traps)
98-100, TRAP LOCKED (TLD) To open test TRAP LOCKED DOOR - Test: Dex -20 (S:Open) (F:-1 Pick, -1 hp, +1 clock) (Locks,Traps)
`;

// https://stackoverflow.com/questions/5034781/js-regex-to-split-by-line
//const lines = table_ee.split(/\r?\n/);    // match empty lines
const lines = table_hl.match(/[^\r\n]+/g);  // does not match empty lines

var rows = [];
var fields = [];
for(var line of lines) {
    // console.log('dline', line);
    const d = parse_door_line(line);    
    if (d !== '') fields.push(d);
    rows.push(line.trim());
}


export default {
    key:'doors',
    title: 'Doors',
    header:h,
    width:th.get_max_width(lines),
    rows:rows,
    fields:fields
};


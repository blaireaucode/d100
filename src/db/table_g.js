/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import * as th from '../table_helpers.js';

// ----------------------------------------------------------------------------
function parse_door_line(line)
{
    // D100
    var a = parseInt(line);
    var b = a;
    if (isNaN(a)) return "";
     line = th.s_rm(line, a);
    if (line[0] === '-') {
        line = th.s_rm(line, "-");
        b = parseInt(line);
        if (isNaN(b)) return "";
        line = th.s_rm(line, b);
    }
    const D100_1 = a;
    const D100_2 = b;
    line = line.trim();
    line = th.s_rm(line, ',');
    line = line.trim();
    // console.log('D100', D100_1, D100_2, line);

    // Name    
    var words = line.split(" ");
    var index = 0;
    var name = '';
    var w = words[index];
    while (w[w.length-1] !== ':') {
        name = name+' '+w;
        index+=1;
        w = words[index];
    }
    name = name+' '+w;
    // console.log('name', name);

    // Details
    const details = line;
    
    return {D100_1:D100_1,
            D100_2:D100_2,
            name:name,
            details:details
    };
}

// ----------------------------------------------------------------------------
const h = 'D100 Name Details';

const table_hl = `

1, GIANT BALL TRAP: Part of the dungeon wall has been rigged to drop down into the floor and release a huge stone ball that will roll itself towards anyone entering the area and crush them. Test - BALL TRAP - Test: Dex-5 (S: Avoid Trap) (F:Belt Check, -6 hp) (Traps,Aware,Lucky) 
2, POISONOUS GAS TRAP: A short vapour of green poisonous gas has been set to billow out from hidden slots in the dungeon floor Test - GAS TRAP - Dex (S: Spotted Trap) (F:-3 hp, +1 clock) (Traps,Aware,Lucky) 
3, PENDULUM TRAP: Several large axes have been suspended  above and rigged to swing out from hidden slots in the dungeon wall . Test - PENDULUM TRAP - Test: Dex -10 (S: Avoid Trap) (F:-4 hp) (Traps,Aware,Lucky) 
4, SNAKE PIT TRAP: Part of the dungeon floor has been rigged to fall away, dropping anyone foolish enough into a deep pit where a giant snake waits for his next meal. Test - SNAKE PIT TRAP - Test: Dex -15 (S: Spotted Trap) (F:Belt Check, -2 hp, encounter GIANT SNAKE, +1 clock) (Traps,Aware,Lucky) GIANT SNAKE - AV:55 DEF:3 HP:16 DMG:+2 (K:Table P +10) (Poison)
5, SPIKED PIT TRAP: Part of the dungeon floor has been rigged to fall away, dropping anyone foolish enough into a deep pit furnished with razor sharp spikes. Test - SPIKED PIT TRAP - Test: Dex (S: Spotted Trap) (F:Belt Check, -2 hp, roll SPIKES, +1 clock) (Traps,Aware,Lucky) SPIKES - Roll 1d10, odds spikes missed, even spikes hit -2 HP.
6, PIT TRAP: Part of the dungeon floor has been rigged to fall away, dropping anyone foolish enough into a deep pit. Test - PIT TRAP - Test: Dex -5 (S:Spotted Trap) (F:Belt Check, -2 hp, +1 clock) (Traps,Aware,Lucky) 
7, CAVE IN TRAP: Part of the ceiling has been rigged to collapse showing the party with rocks and debris. Test - CAVE IN TRAP Test: Dex -10 (S: Spotted Trap) (F:Belt Check, -2 hp, +1 clock) (Traps,Aware,Lucky) 
8, SPEAR TRAP: Spears have been set to shoot out from hidden holes in the dungeon wall. Test - SPEAR TRAP - Test: Dex -15 (S: Spotted Trap) (F:-2 hp) (Traps,Aware,Lucky) 
9, FIRE TRAP: A short burst of scorching hot flame has been set to shoot out from hidden slots in the dungeon wall. Test - FIRE TRAP - Test: Dex -5 (S: Spotted Trap) (F:-1 hp) (Traps,Aware,Lucky) 
10, CROSSFIRE TRAP: Arrows have been set to shoot out from hidden holes in the dungeon wall. Test - CROSSFIRE TRAP - Test: Dex (S: Spotted Trap) (F:-1 hp) (Traps,Aware,Lucky) 
11, CAGE TRAP: A huge cage, hidden from view has been suspended from the ceiling high above and will drop down over an unsuspecting character. Test - CAGE TRAP - Test: Dex-10 (S:Avoid Trap) (F:Trapped, Test LIFT CAGE until free) (Traps,Aware) LIFT CAGE - Test: Str -20 (S:Free) (F:+1 clock) (Strong)
12-15, BARRELS: The area is littered with a number of barrels, if you wish to open a few roll on the table below to determine what you find and add +1 to the time track. 1-3 = You have disturbed a GIANT SPIDER that has made home amongst the barrels you must fight it. 4-5 = You find nothing of interest. 6-7 = You find, roll on TABLE R - REQUIRED 8-9 = You find, roll on TABLE I - ITEM 10= You find, roll on TABLE W - WEAPON GIANT SPIDER - AV:30 DEF:2 HP:6 DMG:-1 (K:Table P) (Web)
16, SPIDERS WEBS: The entire area is covered by thick sticky strands of a giant spider web, making movement through the section time consuming and difficult add +1 clock to the Time Track. Using a spell that creates fire will cancel this penalty. 
17, MOSS: The dungeon floor is completely covered in a damp spongy moss roll 1d10. 1-2 The moss has grown across deep pools of water which break through into the dungeon floor, the edges of which are very sharp, each step you take there is a danger you may fall in and cut yourself on the rock. Test: Dex -10 (S: -1 clock to the Time Track, Use Exits) (F: +2 clock to the Time Track, -2 HP, Use Exits) (Agility,Lucky) 3-4 The moss is very slippery making movement through the section difficult. Test: Dex -5 (S: -1 clock to the Time Track, Use Exits) (F: +2 clock to the Time Track, Use Exits.) (Agility,Lucky) 5-10  The moss hides small boulders and rubble below its surface, you constantly find you are stumbling and in danger of twisting an ankle. Test: Dex (S: Use Exits) (F: +1 clock to the Time Track, Use Exits.) (Agility,Lucky) 
18, ROPE BRIDGE: A huge area of the dungeon floor has at one time collapsed leaving behind a deep void running from the top right hand corner to the bottom left hand corner of this section (mark on map). At some time someone or something has erected a crude rope bridge that now provides the only way across this bottomless gorge. It looks very old and will require some careful manurers to cross safely. CROSS BRIDGE - Test: Dex -5 (S: Use Exits) (F:- all HP) (Agility,Lucky) 
19-22, TRAPPED CHEST: A large wooden chest banded with golden trims stands before you, if you wish to open the chest - Test -  TRAPPED CHEST - Test: Dex -20 (S:Open, TABLE TC+10) (F:-5 HP, +1 clock) ((Traps,Lucky) 
23-24, LEVER: You have found a Lever, if you decide to activate it, roll 1d10 - 1-2 = Something Bad Happens (Roll on Table C) 3-5 = Nothing Happens 6-10 = You hear a rumble somewhere in the dungeon (shade in 1 Lever Pip)
25, LAVA: Pools of flowing lava is gurgling and bubble all around you and every few seconds pieces rock explode into fragments sending hot splashes of lava in all directions. You can just about find a pathway of solid rock to get to all the exits in this section (TEST LAVA PATH) or you can go back from where you came. LAVA PATH - Dex -10 (S:-1HP, Use Exits) (F:-3 HP, Use Exits) (Agility,Lucky) 
26-29, LOCKED CHEST: A large wooden chest banded with golden trims stands before you. Roll 1d10 equal to or less than the number of keys you have to open, if successful delete 1 key; or if you have a pick you may try picking the lock. Test - LOCKED CHEST - Test: Dex -20 (S: Open, TABLE TC) (F:-1 Pick, +1 clock) (Locks,Lucky) 
30, ROPE BRIDGE: A huge area of the dungeon floor has at one time collapsed leaving behind a deep void running from the top left hand corner to the bottom right hand corner of this section (mark on map). At some time someone or something has erected a crude rope bridge that now provides the only way across this bottomless gorge. It looks very old and will require some careful manurers to cross safely. CROSS BRIDGE - Test: Dex -5 (S: Use Exits) (F:- all HP) (Agility,Lucky) 
31, BOTTOMLESS PIT: A deep pit probably a mine shaft blocks the way to all other exits, the pit seems to go on forever, which suggests it’s a waste of time trying to descend. The only way to leave this area other than the way you came is to jump the pit. JUMP PIT - Test: Dex -10 (S: Use Exits) (F:- all HP) (Agility,Lucky) 
32, CAVE IN: The entire ceiling in the area has given way and has buried everything with rock, there does not appear to be anyway to get through. You must retrace your steps for clear the rubble (+3 clock).
33-35, BARRELS: The area is littered with a number of barrels, if you wish to open a few roll on the table below to determine what you find and add +1 to the time track. 1-3 = You have disturbed a GIANT SPIDER that has made home amongst the barrels you must fight it. 4-5 = You find nothing of interest. 6-7 =You find, roll on TABLE I - ITEM 8-9 = You find, roll on TABLE W - WEAPON 10= You find, roll on TABLE A - ARMOUR GIANT SPIDER - AV:30 DEF:2 HP:6 DMG:-1 (K:Table P) (Web)
36-40, LEVER: You have found a Lever, if you decide to activate it, roll 1d10 - 1 = Something Bad Happens (Roll on Table C) 2-4 = Nothing Happens 5-10 = You hear a rumble somewhere in the dungeon (shade in 1 Lever Pip)
41, RIVER: A fast flowing river is running through this section of the dungeon and will need to be crossed to proceed through any exits on the other side. It runs from the top left hand corner to the bottom right hand corner of this section (mark on map) To cross the river test  - SWIM RIVER - Test: STR -10 (S:Use Exits) (F: Exits Blocked, +1 clock) (Strong)
42, TRAPPED CHEST: A large wooden chest banded with silver trims stands before you, if you wish to open the chest - Test - TRAPPED CHEST - Test: Dex -15 (S:Open, TABLE TB+10) (F:-4 HP, +1 clock) (Traps,Lucky) 
43, LOCKED CHEST: A large wooden chest banded with silver trims stands before you. Roll 1d10 equal to or less than the number of keys you have to open, if successful delete 1 key; or if you have a pick you may try picking the lock. Test - LOCKED CHEST - Test: Dex -15 (S: Open, TABLE TB) (F:-1 Pick, +1 clock) (Locks,Lucky) 
44, RIVER: A fast flowing river is running through this section of the dungeon and will need to be crossed to proceed through any exits on the other side. It runs from the top right hand corner to the bottom left hand corner of this section (mark on map). To cross the river test - SWIM RIVER - Test: STR -10 (S:Use Exits) (F: Exits Blocked, +1 clock) (Strong)
45-48, TRAPPED CHEST: A large wooden chest banded with iron trims stands before you, if you wish to open the chest - Test - TRAPPED CHEST - Test: Dex -10 (S:Open, TABLE TA+10) (F:-3 HP, +1 clock) (Traps,Lucky) 
49, PORTCULLIS: A large iron portcullis block the way through this part of the dungeon and will need to be lifted out of the way or the you will be forced to retrace your steps and leave via the way you came. LIFT PORTCULLIS - Test: STR -15 (S:Use Exits) (F: Exits Blocked, +1 clock) (Strong)
50, BARRELS: The area is littered with a number of barrels, if you wish to open a few roll on the table below to determine what you find and add +1 to the time track and you may only roll once. 1-3 = You have disturbed a GIANT SPIDER that has made home amongst the barrels you must fight it. 4-5 = You find nothing of interest. 6-7 =You find, roll on TABLE W - WEAPON 8-9 = You find, roll on TABLE A - ARMOUR 10= You find, roll on TABLE TA - TREASURE A GIANT SPIDER - AV:30 DEF:2 HP:6 DMG:-1 (K:Table P) (Web)
51, BOULDER: A huge boulder block the way through this part of the dungeon and will need to be lifted out of the way or the you will be forced to retrace your steps and leave via the way you came. MOVE BOULDER - Test: STR -10 (S:Use Exits) (F: Exits Blocked, +1 clock) (Strong)
52-53, LEVER: You have found a Lever, if you decide to activate it, roll 1d10 - 1-3 = Nothing Happens 4-10 = You hear a rumble somewhere in the dungeon (shade in 1 Lever Pip)
54, CRYPT: The area is dank and foul smelling, all around you are tombs, some of which have been disturbed and still hold humanoid remains. Two sarcophagus’s catch your eye that have not been opened and if you wish you can spend some time opening them, +1 clock on the time track each time you roll (maximum 2 rolls). 1 = A SKELETON Animates from the tomb and you must fight it. 2-4 = It is empty. 5-10 = Inside apart from some old bones is a Treasure, roll on TABLE TA - TREASURE A SKELETON - AV:50 DEF:4 HP:13 DMG:+1 (K:Table A/W +15) (Fear, Regenerate, Resurrection)
55-58, LOCKED CHEST: A large wooden chest banded with iron trims stands before you. Roll 1d10 equal to or less than the number of keys you have to open, if successful delete 1 key; or if you have a pick you may try picking the lock. Test - LOCKED CHEST - Test: Dex -10 (S: Open, TABLE TA) (F:-1 Pick, +1 clock) (Locks,Lucky) 
59, CHASM: A vast chasm crosses the area, running from the top left hand corner to the bottom right hand corner. of this section (mark on map).  The chasm is so vast and deep it cannot be crossed, exists on the other side of the chasm cannot be used from the side you are on.
60-62, STRANGE CIRCLE: A large circular design has been carved into the dungeon floor, if you wish you may step into it and roll 1d10 - 1 = A surge of energy races through you (Roll on TABLE C - CURSE) 2-3 = TRAP, you Fall into a Pit (Belt Check, -2 hp, +1 clock) 4-5 = There is a flash of light and monster appears (Roll on TABLE E - ENCOUNTER) 6-7 = A powerful force fills your body (Roll on TABLE B - BONUS) 8-10 = You hear a click and the circle design lowers a little into the floor (shade in 1 Lever Pip)
63-64, FORGE: The area was once used as a forge and workshop, a little rusty but some of the weapons and armour it once produced still remain on work benches, after a quick scout through you find a few items that have been completed enough to be of some use. Roll on TABLE A - ARMOUR and TABLE W - WEAPONS twice each to see what you find.
65, CHASM: A vast chasm crosses the area, running from the top right hand corner to the bottom left hand corner of this section (mark on map). The chasm is so vast and deep it cannot be crossed, exists on the other side of the chasm cannot be used from the side you are on.
66-69, FOUNTAIN: You spot a fountain flowing from one of the dungeon walls, if you wish to drink the liquid roll a 1d10 - 1-2 = The foul tasting liquid brings on a sickening gurgle from within (Roll on Table C) 3-6  = Nothing Happens 7-10 = The liquid has a pleasant taste and you feel empowered (Roll on Table B)
70, TREE: Rooted In the centre of this area is a gigantic tree with long spreading branches that blocks out the entire ceiling with bright green leaves. A beautiful white flower grows from most of its stems, they have a pulsating white glow that reminds you of a beating heart. As you walk around the flowers twist and turn so they always face you. If you wish to pick a flower roll 1d10 once. 1 = As you take a flower the leaves attached to its branch fade and turn brown, slowly at first more and more leaves begin to die until the spreading sickness connects with flowers, turning them jet black and reducing their glow until they have no energy at all. In less than a minute the dying leaves are shredding from the now knurled decaying branches and in less than 10 minutes the tree has completely died and is nothing more than a blackened trunk with rotting leafless branches. Wishing you hadn't taken a flower, you curse your bad luck (lose 1 FATE point, if you do not have a FATE point, lose half of your current HP rounded up). 2 = The flower is hot to touch and burns you -1 HP. 3 = When you take a flower it stops glowing and turns black, might be worth a few gold (Value - 2gp). 4-6 = While in your hand the flower continues to glow brightly to a pulsating rhythm, each time you put it down its glow halts. The flower will make an excellent light source the next time your lantern needs refuelling (+1 Oil) 7-10 = As you pick a flower it immediately crystallises, you have heard of this type of tree before, called a “Crystal Tree”. The flowers when picked transforms into a magnificent crystal. You know that they will fetch a high price and pick as many as you can safely carry +300gp.
71, MUSHROOMS: The whole area is home to some a strange looking mushroom, it is growing everywhere and as you move through the stalks twist and turn trying to get nearer to you. They appear to glow at times as if to a rhythmic beat and feel warm to the touch. If you wish to eat some foll 1d10. 1-2 = They taste foul and make you feel quite ill (Roll on Table C - CURSE+20) 3-4 = They have an unpleasant flavour and something seems to stir within you (Roll on Table C - CURSE) 5-6 = Nothing Happens 7-8 = They taste decent enough and you pick a few to eat later (+2 Food) 9-10 = The Mushrooms have a unique flavour and make you a little light headed (Roll on Table B - BONUS)
72-74, BARRELS: The area is littered with a number of barrels, if you wish to open a few roll on the table below to determine what you find and add +1 to the time track. 1-3 = You have disturbed a GIANT SPIDER that has made home amongst the barrels you must fight it. 4-5 = You find nothing of interest. 6-7 =You find, roll on  TABLE A - ARMOUR 8-9 = You find, roll on TABLE TA - TREASURE A 10= You find, roll on TABLE TB - TREASURE B GIANT SPIDER - AV:30 DEF:2 HP:6 DMG:-1 (K:Table P) (Web)
75-77, LEVER: You have found a Lever, if you decide to activate it, roll 1d10 - 1-2 = Nothing Happens 3-10 = You hear a rumble somewhere in the dungeon (shade in 1 Lever Pip)
78, LOCKED CHEST: A large wooden chest banded with wooden trims stands before you. Roll 1d10 equal to or less than the number of keys you have to open, if successful delete 1 key; or if you have a pick you may try picking the lock. Test - LOCKED CHEST - Test: Dex -5 (S: Open, TABLE A) (F:-1 Pick, +1 clock) (Locks,Lucky) 
79, SHRINE: You spot a Shrine devoted to the War God Kantoka, If you wish to spend some time (+1clock) and make a pray roll a 1d10. 1-2 Something does not feel quite right (Roll on Table C) 3-5 Nothing Happens 6-10 You feel that your prayers have been answered (Roll on Table B)
80, TRAPPED CHEST: A large wooden chest banded with wooden trims stands before you, if you wish to open the chest - Test - TRAPPED CHEST - Test: Dex -5 (S:Open, TABLE A+10) (F: -2 HP, +1 clock) (Traps,Lucky) 
81, ALTER: An Alter has been set out for Sacrificial service to Pendra Goddess of Pleasure, If you wish to spend some time (+1clock) and make a pray roll a 1d10, otherwise continue with your quest, you can leave any PART you have collected as an offering, to gain +1 to the roll. 1 Something does not feel quite right (Roll on TABLE C - CURSE) 2-4 Nothing Happens 5-10 You feel that your prayers have been answered (Roll on TABLE B - BONUS)
82-83, ADVENTURER: You come across a friendly adventurer who is leaving the dungeon, after some time chatting about your conquests (+1 clock) he offers to sell you some adventuring items that you may need. Using TABLE R - REQUIRED, you may purchase up to 5 items from him but he asks double the listed price for each. Alternatively you may try and steal the items by making a STEAL test for each item, if you get caught he attacks you (see below) otherwise he bids you farewell and you both continue on your way. STEAL - Test: Dex -15 (S: Take 1 Item) (F: Caught) (Aware) If you are Caught or simply wish to kill the man for his Items an encounter ensues, if the adventurer escapes you lose 1 Reputation. ADVENTURER - AV:55 DEF:3 HP:12 DMG:+2 
84-87, LEVER: You have found a Lever, if you decide to activate it, roll 1d10 - 1 = Nothing Happens 2-10 = You hear a rumble somewhere in the dungeon (shade in 1 Lever Pip)
88-90, STAIRS: The chamber contains a large stair leading down to another part of the dungeon (Mark a Stairs  on the Map). You may descend the stairs from this section at any time by taking another sheet of mapping paper and numbering it, then draw an identical section to this in the same grid location as this and add a stair to that section, lastly mark the stairs as down to page X and Up to page X as applicable, with X referencing the maps page number. Each time you traverse a stair add +1 clock to the Time Track.
91-92, LOCKED CHEST: A large wooden chest stands before you. Roll 1d10 equal to or less than the number of keys you have to open, if successful delete 1 key; or if you have a pick you may try picking the lock. Test - LOCKED CHEST - Test: Dex (S: Open, TABLE W) (F:-1 Pick, +1 clock) (Locks,Lucky) 
93-95, TRAPPED CHEST: A large wooden chest stands before you, if you wish to open the chest - Test - TRAPPED CHEST - Test: Dex (S:Open, TABLE W+10) (F:-1 HP, +1 clock) (Traps,Lucky) 
96-97, GRATE: Set in the floor is a small grate, almost like a drain except you cannot see a drainage hole. Its pretty dusty and a lot of debris has managed to find its way into the small pit below, but something does catch you eye and if you can remove the grate you can lean in and reach it. Test - LIFT GRATE - Test: Str -10 (S:FIND) (F:+1 clock) (Strong) FIND roll 1d10 1-4 Nothing of Interest. 5-8 Roll on TABLE I - ITEMS 9-10 Roll on TABLE TA - TREASURE A
98, TREASURE ROOM: You have found a treasure room, roll 1d10 and multiply the result by 5 to discover how much gold you find and then roll on TABLE TA once (x1).
99, TREASURE ROOM: You have found a treasure room, roll 1d10 and multiply the result by 10 to discover how much gold you find and then roll on TABLE TB once (x1) or TABLE TA twice (x2). 
100, TREASURE ROOM: You have found a treasure room, roll 1d10 and multiply the result by 20 to discover how much gold you find and then roll on Table TC once (x1) or TABLE TB twice (x2) or TABLE TA thrice (x3).

`;

// https://stackoverflow.com/questions/5034781/js-regex-to-split-by-line
//const lines = table_ee.split(/\r?\n/);    // match empty lines
const lines = table_hl.match(/[^\r\n]+/g);  // does not match empty lines

var rows = [];
var fields = [];
for(var line of lines) {
    // console.log('dline', line);
    const d = parse_door_line(line);    
    if (d !== '') fields.push(d);
    rows.push(line.trim());
}


export default {
    key:'geographic',
    title: 'Geographic',
    header:h,
    width:th.get_max_width(lines),
    rows:rows,
    fields:fields
};


/*
 * Copyright 2018
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

export const SET = "SET";
export const SET_ITEMS = "SET_ITEMS";
export const ADVENTURER_SET = "ADVENTURER_SET";
export const ENCOUNTER_SET = "ENCOUNTER_SET";
export const DUNGEON_SET = "DUNGEON_SET";
export const TEST_SET  = "TEST_SET";

/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class CombatStatus extends Component {

    render() {
        const area = this.props.dungeon.area;
        if (!area) return ('');
        const combat = area.combat;
        if (!combat) return ('');
        const step = combat.step;
        if (!step) return ('');

        // if (step === 'start') return ('');

        // --> replace \n by <br/> ?
        
        return (
            <div className='combat-status'>         
              Round: {combat.round} {' '}
              <br/>
              {combat.status}              
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CombatStatus);


/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

/*
  - get_max_width(lines)
  - s_rm(line, v)
  
*/

// ----------------------------------------------------------------------------
export function get_max_width(lines)
{
    var w = 0;
    for(var line of lines)
        w = (line.length>w ? line.length:w);
    return w;
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function s_rm(line, v)
{
    // remove from the line the len(v) first char
    const l = line.substring(v.toString().length).trimLeft();
    return l;
}
// ----------------------------------------------------------------------------



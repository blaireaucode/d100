/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import DiceTest from './DiceTest.js';
import DiceTestHelp from './DiceTestHelp.js';
import * as ch from './combat_helpers.js';
import update from 'immutability-helper';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class CombatActionAdvDmg extends Component {

    constructor(props) {
        super(props);
        this.onDmgCompleted = this.onDmgCompleted.bind(this);
        this.onDmgStarted = this.onDmgStarted.bind(this);
    }
    
    onDmgStarted() {
        var test = ch.new_test('test_adv_dmg', 6);
        test.dices.push({dice_max:10});
        return test;
    }
    
    onDmgCompleted(test) {
        const d10 = test.dices[1].value;
        const d6 = test.dices[0].value;
        const hl = ch.get_hit_location(d10);
        var hit_dmg_mod = hl.DmgMod;
        if (hit_dmg_mod === 'Check') {
            hit_dmg_mod = 0;
            console.log('TODO Belt Check !!!!!!!', hit_dmg_mod);
        }
        else hit_dmg_mod = parseInt(hit_dmg_mod, 10);

        // dmg
        const encounter = this.props.encounter;
        const adv_dgm_mod = this.props.adventurer.dmg; // FIXME 
        const def = parseInt(encounter.def, 10);
        var dmg = hit_dmg_mod + d6 + adv_dgm_mod - def;
        if (dmg < 0) dmg = 0;
        const hp =  parseInt(encounter.hp - dmg);
        var ee = update(encounter, {hp: {$set:hp}});
        this.props.set('encounter', ee);

        // status
        const s1 = 'Roll hit location = '+hl.Location+
              ' (HitDmgMod is '+hit_dmg_mod+')';
        const s2 = 'Total damage is D6 + HitDmgMod + dmg - monster def = '+d6+
              ' + '+hit_dmg_mod+' + '+adv_dgm_mod+
              ' - '+def+' = '+dmg;
        const l = update(this.props.log, {$push:[s1,s2]});
        this.props.set('log', l);
    }
    
    render() {
        return (
            <span>
              <DiceTestHelp id={'test_adv_dmg'}>
                
                Roll D10 to get the dmg modifier (HitDmgMod) from the
                Hit Location Table.<br/>
                
                Then roll D6 to get the Dmg. <br/>
                
                To obtain the total Dmg, compute D6 + HitDmgMod + Dmg - Def
              
              </DiceTestHelp> {' '}
              <DiceTest
                id='test_adv_dmg'
                onRollingStarted={this.onDmgStarted}
                onRollingCompleted={this.onDmgCompleted}>
                Roll damage to monster
              </DiceTest>
            </span>
        );
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(CombatActionAdvDmg);


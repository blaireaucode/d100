
/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import List from '@material-ui/core/List';
import ListItemText from '@material-ui/core/ListItemText';
import Paper from '@material-ui/core/Paper';
import L from './L.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class LogPanel extends Component {

    resetLog() {
        this.props.set('log', []);
    }
    
    render() {

        var items = [];
        const log = this.props.log;
        for(var i = log.length ; i>=0; i--) {
            items.push( <ListItemText key={i}>
                        <span className={'log'}>
                        {log[i]}
                        </span>
                        </ListItemText>);
        }

        var l ='';
        if (log.length>0)
            l = <L onClick={this.resetLog.bind(this)}>
                  ~ Erase log ~
                </L>;        
        return (
            <div 
              className='log-div'
              style={{textAlign:'center'}}>
              {l}
              <Paper
                className='log'
                style={{maxHeight: '200px',
                        textAlign: 'left',
                        overflow: 'auto'}}>
                <List>
                  {items}
                </List>
              </Paper>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LogPanel);

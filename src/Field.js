
/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { RIEInput, RIENumber} from '@attently/riek';
import update from 'immutability-helper';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class Field extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: this.props.adventurer[this.props.name]
        };
    }

    onChange = (newState) => {
        console.log('Field change', this.state, newState);
        const adv = this.props.adventurer;
        const n = this.props.name;
        var v = newState.value;
        if (this.props.type === 'n')
            v = parseInt(v, 10);
        this.setState(newState);
        const a = update(adv, {[n]:{$set:v}});
        this.props.adventurer_set(a);
    };

    validateInt(a) {
        // console.log('validate', a, this.state);
        if (a === '') return false;
        return true;
    } 
 
    render() {
        console.log('Field render', this.props.adventurer);
        if (this.props.type === 'n') return this.renderNumber();
        return (
            <span>
              {this.props.name}: {' '}
              <RIEInput
                classEditing={'fieldEdit'}
                className={'field'}
                value={this.state.value}
                change={this.onChange}
                propName='value'
              />
            </span>
        );
    }
    
    renderNumber() {
        return (
            <span>
              {this.props.name}: {' '}
              <RIENumber
                classEditing={'fieldEdit'}
                className={'field'}
                value={this.state.value}
                change={this.onChange}
                validate={this.validateInt.bind(this)}
                propName='value'
              />
            </span>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Field);

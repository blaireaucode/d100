/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import DiceTest from './DiceTest.js';
import update from 'immutability-helper';
import * as dh from './d100_helpers.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class CombatActionEncounterAttack extends Component {

    constructor(props) {
        super(props);
        this.onEncAttackTestCompleted = this.onEncAttackTestCompleted.bind(this);
        this.onEncAttackTestStarted = this.onEncAttackTestStarted.bind(this);
    }
    
    onEncAttackTestStarted() {
         const test = {
            id:'test_enc_att',
            state:'init',
            dices:[{dice_max:100}], 
        };
        return test;
    }
    
    onEncAttackTestCompleted(test) {
        const dungeon = this.props.dungeon;
        const combat = dungeon.area.combat;
        const att = dh.get_enc_attack(combat.encounter, test);
        var dd = update(dungeon,
                        {area: {combat: {enc_att: {$set:att}}}});

        // Next combat step FIXME check S or F
        dd = update(dd,
                    {area: {combat: {step: {$set:'step_enc_dmg'}}}});

        // status
        const s = (att.result === 'S' ? 'Success':'Fail');
        dd = update(dd,
                    {area: {combat: {status: {$set:s}}}});
        
        this.props.dungeon_set(dd);        
    }
    
    render() {
        const area = this.props.dungeon.area;
        if (!area) return ('');
        const combat = area.combat;
        if (!combat) return ('');
        const step = combat.step;
        if (!step) return ('');

        return (
            <DiceTest
              id='test_enc_att'
              onRollingStarted={this.onEncAttackTestStarted}
              onRollingCompleted={this.onEncAttackTestCompleted}>
              Roll to get the monster attack ...
            </DiceTest>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CombatActionEncounterAttack);


/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { HashRouter as Router } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';
import Layout from './Layout.js';
import Test from './Test.js';
import Config from './Config.js';
import Adventurer from './Adventurer.js';
import Quest from './Quest.js';
import Dungeon from './Dungeon.js';
// import Combat from './Combat.js';
import Combat2 from './Combat2.js';
import About from './About.js';
import Debug from './Debug.js';

class Routes extends Component {

    render() {
        //console.log('Public url', process.env.PUBLIC_URL);
        return (
            <Router basename={process.env.PUBLIC_URL}>
              <div>
                <Layout path="/adventurer/" component={Adventurer} />
                <Layout path="/quest/" component={Quest} />
                <Layout path="/dungeon/" component={Dungeon} />
                {/* <Layout path="/combat_old/" component={Combat} /> */}
                <Layout path="/combat/" component={Combat2} />
                <Layout path="/about/" component={About} />
                <Layout path="/debug/" component={Debug} />
                <Layout path="/config/" component={Config} />
                <Layout exact path="/" component={Test} />
              </div>
            </Router>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Routes);


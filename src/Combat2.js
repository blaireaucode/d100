/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import CombatEncounter2 from './CombatEncounter2.js';
import CombatAdventurer2 from './CombatAdventurer2.js';
import CombatActions2 from './CombatActions2.js';
import LogPanel from './LogPanel.js';
import Field3 from './Field3.js';
import Grid from '@material-ui/core/Grid';

import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class Combat2 extends Component {

    render() {
        
        return (
            <div className='combat-layout'>
              <Grid
                container
                direction="row"
                justify="space-evenly"
                alignItems="flex-start">
                
                <Grid item className='combat-layout'>
                  <Field3 o='quest' f='enc_mod' type='number'/>
                  <hr/>
                  <CombatAdventurer2/>
                </Grid>

                <Grid item className='combat-layout'>
                  <CombatEncounter2/>
                </Grid>
                
              </Grid>

              <Grid
                container
                direction="row"
                justify="flex-start"
                alignItems="flex-start">                
                <Grid item xs={12} className='combat-layout-actions'>
                  <br/>
                  <CombatActions2/>                  
                  <LogPanel/>
                </Grid>
                
              </Grid>
            </div>
        );
    }    
}

export default connect(mapStateToProps, mapDispatchToProps)(Combat2);


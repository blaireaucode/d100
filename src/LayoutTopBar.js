/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React from 'react';
import PropTypes from 'prop-types';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import { withStyles } from '@material-ui/core/styles';
//import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
// import L from './L.js';
// import ArrowBack from '@material-ui/icons/ArrowBack';
// import ArrowForward from '@material-ui/icons/ArrowForward';
//import Delete from '@material-ui/icons/Delete';
// import Cached from '@material-ui/icons/Cached';
// import update from 'immutability-helper';
// import Button from '@material-ui/core/Button';
// import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';

const styles = theme => ({
    menuButton: {
        marginRight: 20,
        // borderBottom: '1px solid',
        // borderColor: '#1b1e23',
        // shadow: 'none',
        // boxShadow: 'none',
        //maxWidth: '1200px',
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        }
    }
});

class LayoutTopBar extends React.Component {

    clearPassage() {
        // const c = this.props.book.current_passage;
        // // console.log('clear passage', c);
        // // set the p[c] to the empty
        // const newp = update(this.props.book.p, { [c]: { $set: {} } });
        // const newb = update(this.props.book, {p: {$set:newp}});
        // this.props.book_set(newb);
    }

    render() {
        //console.log('LayoutTopBar title', this.props.book.current_passage);
        const { classes } = this.props;
        // const cur = parseInt(this.props.book.current_passage,10);
        // const p_previous = cur-1;
        // const p_next = cur+1;
        // var title_passage = this.props.book.current_passage;
        // if (isNaN(title_passage)) title_passage = '0';
        // if (title_passage[0] === '0') title_passage = '0';

        //<img width='15px' src={require('./images/lw5.png')}/>
        // const icon_style = {
        //     verticalAlign: 'middle',
        // };
        // const icon_trash_style = {
        //     verticalAlign: 'middle',
        //     fontSize: '1.2em'
        // };
        
        return (
            <Toolbar  className='topbar'>
              <IconButton
                color="inherit"
                aria-label="Open drawer"
                onClick={this.props.handleDrawerToggle}
                className={classes.menuButton}>
                <MenuIcon />
              </IconButton>
              <Grid container>
               ici
              </Grid>
            </Toolbar>
        );
    }
}

LayoutTopBar.propTypes = {
    classes: PropTypes.object.isRequired,
    // Injected by the documentation to work in an iframe.
    // You won't need it on your project.
    container: PropTypes.object,
    theme: PropTypes.object.isRequired,
};

//export default withStyles(styles, { withTheme: true })(connect(mapStateToProps, mapDispatchToProps)(LayoutTopBar));
export default withStyles(styles, { withTheme: true })(LayoutTopBar);

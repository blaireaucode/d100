/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import update from 'immutability-helper';

// tables
import table_ee from './db/table_ee.js';
import table_hl from './db/table_hl.js';


// ----------------------------------------------------------------------------
/*

  new_test('test_adv_att', 100) // state is init

  at = get_adv_attack(adventurer, test)
  d = set_combat_adv_attack(d, at)

  e = copy_encounter(d) // to be able to make change
  e = get_encounter(test)

  hl = get_hit_location(test)
  
  d = set_combat_encounter(e) // once updated from HP, etc
  d = set_combat_next_step('step_av_dmg')
  d = set_combat_status(txt)
  d = set_combat_next_round()

  dmg, status = get_adv_dmg(adv, enc, test)
  dmg, status = get_enc_dmg(adv, enc, test)
  
*/
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function new_test(name, dice_max)
{
    const test = {
        id:name,
        state:'init',
        dices:[{dice_max:dice_max}]
    };
    return test;
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function get_encounter(test, quest)
{
    const d = test.dices[0].value+(quest.enc_mod || 0);

    // parse dice value
    // console.log('table ee', d, table_ee);
    for(var enc of table_ee.fields) {
        // console.log('enc', enc);
        if (d>= enc.D100_1 && d<=enc.D100_2)
            return enc;
    }
    console.log('ERROR get_encounter', test, table_ee);
    return '';
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function set_combat_encounter(dungeon, encounter)
{
    const d = update(dungeon,
                     {area:
                      {combat:
                       {encounter: {$set:encounter}}}});
    return d;
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function set_combat_next_step(dungeon, next_step)
{
    const d = update(dungeon,
                     {area:
                      {combat:
                       {step: {$set:next_step}}}});
    return d;
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function set_combat_next_round(dungeon)
{
    const r = dungeon.area.combat.round+1 || 1;
    const d = update(dungeon,
                     {area:
                      {combat:
                       {round: {$set:r}}}});
    return d;
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function get_adv_attack(adv, test)
{
    const v = test.dices[0].value;

    // FIXME add bonus etc
    const success = (v <= adv.strength);
    var att = { result: success? 'S':'F' };
    
    return att;
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function set_combat_adv_attack(dungeon, att)
{
    const d = update(dungeon,
                     {area:
                      {combat:
                       {adv_att: {$set:att}}}});
    return d;
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function set_combat_status(dungeon, status)
{
    const d = update(dungeon,
                     {area:
                      {combat:
                       {status: {$set:status}}}});
    return d;
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function get_hit_location(value)
{
    // parse dice value
    console.log('hit locat', value,table_hl.fields);
    var l = table_hl.fields[value];
    console.log('hit', l);

    // Parse location and dmg_mod
    l = l.trim();
    var words = l.split(" ");
    var index = 0;
    var Location = '';
    while (index<words.length-1) { // last word is DmgMod
        Location = Location+' '+ words[index];
        index = index+1;
    }
    Location = Location.trim();

    // Part DmgMod (not always a number)
    const DmgMod = words[index];
    
    const hl = { dice_value:value, Location:Location, DmgMod:DmgMod};
    return hl;
}
// ----------------------------------------------------------------------------

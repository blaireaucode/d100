/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Table from './Table.js';
import DefaultDices from './DefaultDices.js';
import CombatActionNewEncounter from './CombatActionNewEncounter.js';
import CombatActionAdvAttack from './CombatActionAdvAttack.js';
import CombatActionEncDmg from './CombatActionEncDmg.js';
import CombatActionAdvDmg from './CombatActionAdvDmg.js';
import L from './L.js';
import Grid from '@material-ui/core/Grid';

import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class CombatActions2 extends Component {

    removeEnc() {
        this.props.set('encounter', {});
    }
    
    render() {
        const s = 4;
        return (
            <div className='combat-layout'>
              <Grid
                container
                direction="row"
            /* justify="space-evenly" */
                justify="flex-start"
                alignItems="flex-start">
                
                <Grid item xs={s}>
                  <Table t={'enc_reaction'}/>
                </Grid>
                
                <Grid item xs={s}>
                  <Table t={'hit_location'}/>
                </Grid>
                
                <Grid item xs={s}>
                  <Table t={'encounters'}/>
                </Grid>
                
                <Grid item xs={s}>
                  <Table t={'encounter_abilities'}/>
                </Grid>
                
                <Grid item xs={s}>
                  <DefaultDices/>
                </Grid>

                <Grid item xs={s}>
                  <L onClick={this.removeEnc.bind(this)}>
                    Remove encounter
                  </L>
                </Grid>
                
                <Grid item xs={s}>
                  <CombatActionNewEncounter/>
                </Grid>
                
                <Grid item xs={s}>
                  <CombatActionAdvAttack/>
                </Grid>
                
                <Grid item xs={s}>
                  <CombatActionAdvDmg/>
                </Grid>
                
                <Grid item xs={s}>
                  <CombatActionEncDmg/>
                </Grid>
                
              </Grid>
            </div>
        );
    }    
}

export default connect(mapStateToProps, mapDispatchToProps)(CombatActions2);


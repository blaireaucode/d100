
/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import DiceTestHelp from './DiceTestHelp.js';
import Grid from '@material-ui/core/Grid';
import Input from '@material-ui/core/Input';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class EncounterField extends Component {

    render() {
        const area = this.props.dungeon.area;
        const encounter = area.combat.encounter;
        const v = encounter[this.props.name] || '?';
        const inputProps = {
            className: 'field',
            disabled: true,
            value:v
        };

        return (
            <Grid
              container
              direction="row"
              justify="flex-start"
              alignItems="flex-start">
              <Grid item xs={6}>
                {this.props.name} {' '}
                <DiceTestHelp>
                  {this.props.children}
                </DiceTestHelp>
              </Grid>
              <Grid item xs={6}>
                <Input
                  id="time"
                  disableUnderline={true}
                  inputProps={inputProps}
                />
              </Grid>
            </Grid>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EncounterField);

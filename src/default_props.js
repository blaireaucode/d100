/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import {
    set,
    set_items,
    adventurer_set,
    encounter_set,
    dungeon_set,
    test_set
} from './actions.js';

export const mapDispatchToProps = (dispatch) => ({
    adventurer_set(adventurer) { dispatch(adventurer_set(adventurer)); },
    encounter_set(encounter) { dispatch(encounter_set(encounter)); },
    dungeon_set(dungeon) { dispatch(dungeon_set(dungeon)); },
    test_set(id, test) { dispatch(test_set(id,test)); },
    set(element_name, value) { dispatch(set(element_name,value)); },
    set_items(value) { dispatch(set_items(value)); },
});

export const mapStateToProps = store => {
    return {
        adventurer: store.adventurer,
        log: store.log,
        encounter: store.encounter,
        quest: store.quest,
        items: store.items,
        dungeon: store.dungeon,
        area: store.area,
        tests: store.tests
    };
};

/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

/*
  get_label(key)  
*/

// ----------------------------------------------------------------------------
const labels_raw = `
name, Name
type, Type
av, AV, Attack Value
def, Def, Defence
dmg, Dmg, Damage
hp, HP, Health Points
loot, Loot
abilities, Abilities
str, Str, Strengh
dex, Dex, Dexterity
int, Int, Intelligence
armour, A, Armour
shield, S, Shield
enc_mod, Quest mod, Quest modifier
hero_path, Hero Path
race, Race
rep, Reputation
fate, Fate
life, Life
gp, Gold Pieces
`;
const lines = labels_raw.match(/[^\r\n]+/g);  // does not match empty lines
const labels = [];
for(var line of lines) {
    line = line.trim();
    const w = line.split(',');
    // console.log('w',w);
    const key = w[0];
    const short = w[1];
    const long = w[2] || short;
    labels[w[0]] = { key:key, short:short, long:long };
}

export function get_label(key)
{
    if (key in labels) return labels[key];
    return { key:key, short:key, long:key };
}
// ----------------------------------------------------------------------------


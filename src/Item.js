/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import DiceTest from './DiceTest.js';
import Field4 from './Field4.js';
import Grid from '@material-ui/core/Grid';
import * as dh from './d100_helpers.js';
import update from 'immutability-helper';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class Item extends Component {

    constructor(props) {
        super(props);
    }

    /*
      slot item(name) str dex int hp dmg def gp fix A/S damage
      
    */
    
    render() {
        console.log('item', this.props.item);

        const e = { i:this.props.item, nolabel:true, nohelp:true};
        return (
            <span>
              <Grid
                container
                direction="row"
                justify="flex-start"
                alignItems="flex-start">

                <Grid item>
                  <Field4 f={'slot'} {...e} />
                </Grid>

                <Grid item>
                  <Field4 f={'name'} {...e} />
                </Grid>

                <Grid item>
                  <Field4 f={'str'} {...e} />
                </Grid>

                <Grid item>
                  <Field4 f={'dex'} {...e} />
                </Grid>

                <Grid item>
                  <Field4 f={'int'} {...e} />
                </Grid>

                <Grid item>
                  <Field4 f={'hp'} {...e} />
                </Grid>

                <Grid item>
                  <Field4 f={'dmg'} {...e} />
                </Grid>

                <Grid item>
                  <Field4 f={'def'} {...e} />
                </Grid>

                <Grid item>
                  <Field4 f={'gp'} {...e} />
                </Grid>

                <Grid item>
                  <Field4 f={'fix'} {...e} />
                </Grid>

                <Grid item>
                  <Field4 f={'as'} {...e} />
                </Grid>

              </Grid>
            </span>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Item);


/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import DiceTest from './DiceTest.js';
import DiceTestHelp from './DiceTestHelp.js';
import update from 'immutability-helper';
import * as dh from './d100_helpers.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class CombatActionEncounterDmg extends Component {

    constructor(props) {
        super(props);
        this.onDmgCompleted = this.onDmgCompleted.bind(this);
        this.onDmgStarted = this.onDmgStarted.bind(this);
    }

    onDmgStarted() {
        const test = {
            id:'test_enc_dmg',
            state:'init',
            dices:[{dice_max:6}, {dice_max:10}],
        };
        return test;
    }

    onDmgCompleted(test) {
        const dungeon = this.props.dungeon;
        const combat = dungeon.area.combat;
        const encounter = combat.encounter;
        const e = combat.enc_att;
        if (!e) return;
        // var att = JSON.parse(JSON.stringify(e));

        // D6 + hit_dmg_mod + enc_dgm_mod - adv_armour = DMG

        // FIXME--> to put in helper
        const d10 = test.dices[1].value;
        const d6 = test.dices[0].value;
        const hl = dh.get_hit_location(d10);
        var hit_dmg_mod = hl.DmgMod;
        if (hit_dmg_mod === 'Check') {
            hit_dmg_mod = 0;
            console.log('TODO Belt Check !!!!!!!', hit_dmg_mod);
        }
        else hit_dmg_mod = parseInt(hit_dmg_mod, 10);

        // dmg
        const enc_dgm_mod = parseInt(encounter.Dmg, 10);
        const adv_armour = 0; // FIXME 
        var dmg = d6 + enc_dgm_mod + hit_dmg_mod - adv_armour;
        if (dmg < 0) dmg = 0;
        const hp = this.props.adventurer.HP - dmg;
        var adv = update(this.props.adventurer,
                          {HP: {$set:hp}});
        this.props.adventurer_set(adv);

        // status
        const s = 'Hit location = '+hl.Location+
              '  dmg is '+d6+
              ' + '+hit_dmg_mod+' + '+enc_dgm_mod+
              ' - '+adv_armour+' = '+dmg;
        var dd = update(dungeon,
                    {area: {combat: {status: {$set:s}}}});

        // end round and check dead
        const r = combat.round+1;
        dd = update(dd,
                    {area: {combat: {round: {$set:r}}}});

        // step
        dd = update(dd,
                    {area: {combat: {step: {$set:'step_enc_reac'}}}});
        this.props.dungeon_set(dd);
    }


    render() {
        const area = this.props.dungeon.area;
        if (!area) return ('');
        const combat = area.combat;
        if (!combat) return ('');
        const step = combat.step;
        if (!step) return ('');
        const enc_reac = combat.enc_reac;
        if (!enc_reac) return ('');

        // DEBUG 
        //if (enc_reac.dice_value >7) return this.renderMonsterEscape(combat);

        return (
            <span>
              <DiceTest
                id='test_enc_dmg'
                onRollingStarted={this.onDmgStarted}
                onRollingCompleted={this.onDmgCompleted}>
                Success !! Roll to see the damage ... 
              </DiceTest>
              <DiceTestHelp id='test_enc_dmg'>
                
                Roll both the <u>damage die (1d6)</u> and the <u>location die
                (1d10)</u> together. <br/><br/>

                Apply the damage modifier to the <u>damage
                die</u> for the location rolled (see Hit Location
                table) and the Encounters DMG modifier (if
                any TODO). <br/><br/>
                
                Then deduct the Monster DEF value
                 = {combat.encounter.Def} from the modified damage
                dice. <br/><br/>

                The remaining amount is the number of HP that
                are dealt to the Monster or Monsters if encountering
                more than one (see Monster Ability - Pack)
                
              </DiceTestHelp>
            </span>
        );
    }

    renderMonsterEscape(combat) {

        return (
            <div className='dungeon'>
              escape
            </div>
        );
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(CombatActionEncounterDmg);

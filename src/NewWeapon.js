/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import DiceTest from './DiceTest.js';
import * as dh from './d100_helpers.js';
import update from 'immutability-helper';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class NewWeapon extends Component {

    constructor(props) {
        super(props);
        this.onNewWeaponTestCompleted = this.onNewWeaponTestCompleted.bind(this);
        this.onNewWeaponTestStarted = this.onNewWeaponTestStarted.bind(this);
    }
    
    onNewWeaponTestStarted() {
        // FIXME --> modify according to quest etc ...
        return dh.new_test('test_new_weapon', 100);
    }
    
    onNewWeaponTestCompleted(test) {
        const w = dh.get_weapon(test);
        console.log('w',w);
        console.log('ppush it', this.props.items);
        const i = update(this.props.items, {$push:[w]}); // FIXME check if exist ?
        console.log('ppush it after ', i);
        this.props.set('items', i);
        // const v = parseInt(test.dices[0].value,10);
        // const m = parseInt(this.props.quest.enc_mod || 0);
        // var log = 'New weapon '+v+' (roll) + '+m+' (quest mod) = '+(v+m);
        // log += ' : '+e.name;
        // const l = update(this.props.log, {$push:[log]});
        // this.props.set('log', l);      
        
    }
    
    render() {
        return (
            <DiceTest
              id='test_new_weapon'
              onRollingStarted={this.onNewWeaponTestStarted}
              onRollingCompleted={this.onNewWeaponTestCompleted}>
              New weapon
            </DiceTest>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewWeapon);


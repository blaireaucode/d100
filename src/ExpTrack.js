/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';
import update from 'immutability-helper';
import Slider from '@material-ui/core/Slider';
import Grid from '@material-ui/core/Grid';
import L from './L.js';
import * as dh from './d100_helpers';

class ExpTrack extends Component {

    constructor(props) {
        super(props);
        this.onUp = this.onUp.bind(this);
    }
    
    onChange = (event, value) => {
        var v = parseInt(value, 10);
        const elem = dh.field_get_element(this.props);
        const ee = update(elem, {[this.props.f]:{$set:v}});
        dh.field_set_element(this.props, ee);
    }

    onUp() {
        const elem = dh.field_get_element(this.props);
        // set exp to zero
        var ee = update(elem, {[this.props.f]:{$set:0}});
        // add +5 to characteristic
        const l = this.props.f.length-4;
        const n = this.props.f.substring(0,l);
        const v = parseInt(ee[n],10) + 5;
        ee = update(ee, {[n]:{$set:v}});
        dh.field_set_element(this.props, ee);
    }
    
    render() {
        const elem = dh.field_get_element(this.props);
        const value = elem[this.props.f];
        // console.log('value ', this.props.f, value);
        const s = { width:'150px' };
        const classes = {
            root:'slider',
            rail:'slider-rail',
            track:'slider-track',
            markActive:'slider-mark-active',
            valueLabel: 'slider-value-label',
            thumb:'slider-thumb'
        };

        var v = value;
        if (value === 10)
            v = <span>
                  <L onClick={this.onUp}>
                    <span className='hide'></span> &uarr; up
                  </L>
                </span>;
        else
            v = <span><span className='hide'></span>{v}</span>;
        // v += '/10';
        
        return (<span>
                  <Grid
                    container
                    direction="row"
                    spacing={3}
                    justify="flex-start"
                    alignItems="flex-start">
                    <Grid item>
                      <span className='hide'>_</span>
                      <Slider
                        style={s}
                        classes={classes}
                        value={value}
                /* getAriaValueText={valuetext} */
                        aria-labelledby="discrete-slider"
                        onChange={this.onChange}
                        valueLabelDisplay="auto"
                        step={1}
                        marks
                        min={0}
                        max={10}
                      />
                    </Grid>
                    <Grid item>
                      {v}              
                    </Grid>            
                  </Grid>
                </span>);
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ExpTrack);

/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import FieldInput from './FieldInput.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';
import * as charac from './characteristics.js';
import * as dh from './d100_helpers';

class Field4 extends Component {

    render() {
        // characteristic
        const c = charac.characteristics[this.props.f];
        if (!c) return (<span>Erreur {this.props.f}</span>);

        // label
        var label_txt = '';
        var lw = '';
        if (this.props.nolabel) {
            lw = '0ch';
        }
        else{
            label_txt = c.label;
            if (this.props.short_label)
                label_txt = c.short;
            lw = (this.props.lw ? this.props.lw:2+label_txt.length+'ch');
        }
        const labelStyle = {width: lw, textAlign:'left'};
        
        // type 
        const type = c.type;

        // help
        const h = dh.get_field_help(this.props);
        
        // value props
        const element = dh.field_get_element(this.props);
        var value = element[this.props.f];
        if (c.to_str) value = c.to_str(value);
        var vw = '10ch';
        if (this.props.vw) vw = this.props.vw;
        if (vw === 'auto') vw = value.length+2+'ch';

        // align value
        const textAlign = (type === 'number' ? 'right':'left');

        // props
        const inputProps = {
            className: 'fieldEdit',
            disabled: false,
            type: type,
            style: {width: vw, textAlign:textAlign}
        };

        // before
        var before='';
        if (this.props.before) before = this.props.before;
        var after='';
        if (this.props.after) after = this.props.after;

        // adj
        const adj = dh.get_field_adj(this.props);
        
        return (
            <Grid
              container
              direction="row"
              justify="flex-start"
              alignItems="flex-start">

              {before}
              <Grid item style={labelStyle}>
                {h} {label_txt} 
              </Grid>
              
              <Grid item>
                <FieldInput
                  {...this.props} 
                  inputProps={inputProps}/>
              </Grid>

              {adj}
              {after}
              
            </Grid>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Field4);

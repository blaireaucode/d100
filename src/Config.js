/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import L from './L.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';
import default_encounter from './default_encounter.js';
import default_adventurer from './default_adventurer.js';

class Config extends Component {

    constructor(props) {
        super(props);
        this.resetDungeon = this.resetDungeon.bind(this);
        this.resetLog = this.resetLog.bind(this);
        this.resetAdv = this.resetAdv.bind(this);
        this.resetArea = this.resetArea.bind(this);        
        this.resetEncounter = this.resetEncounter.bind(this);
        this.resetTests = this.resetTests.bind(this);
        this.resetItems = this.resetItems.bind(this);        
        this.resetAll = this.resetAll.bind(this);
    }

    resetAll() {
        this.props.set('adventurer', {});
        this.props.set('encounter', {});
        this.props.set('log', []);
        this.props.set('dungeon', {});
        this.props.set('area', {});
        this.props.set('quest', {});
        this.props.set('tests', {});
        global.localStorage.clear();
    }
    

    resetDungeon() {
        this.props.dungeon_set({});
    }
    
    resetArea() {
        this.props.set('area', {});
    }
    
    resetItems() {
        this.props.set('items', []);
    }
    
    resetLog() {
        this.props.set('log', []);
    }
    
    resetAdv() {
        this.props.adventurer_set(default_adventurer);
    }
    
    resetEncounter() {
        this.props.encounter_set(default_encounter);
    }
    
    resetTests() {
        console.log('tests', this.props.tests);
        for (var t in this.props.tests) {
            console.log('test ',t);
            this.props.test_set(t, {});
        }
        // console.log('tests', this.props.tests);
   }
    
    render() {
        return (
            <div className='dungeon'>              
              Config
              <p/>

              <L onClick={this.resetLog}>
                Reset Log
              </L>
              <p/>

              <L onClick={this.resetDungeon}>
                Reset dungeon
              </L>
              <p/>

              <L onClick={this.resetArea}>
                Reset area
              </L>
              <p/>

              <L onClick={this.resetAdv}>
                Default adventurer
              </L>
              <p/>

              <L onClick={this.resetEncounter}>
                Default Encounter
              </L>
              <p/>

              <L onClick={this.resetTests}>
                Reset tests
              </L>
              <p/>

              <L onClick={this.resetItems}>
                Reset items
              </L>
              <p/>

              <L onClick={this.resetAll}>
                Reset all
              </L>

              
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Config);


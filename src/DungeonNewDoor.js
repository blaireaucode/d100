/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import DiceTestHelp from './DiceTestHelp.js';
import DiceTest from './DiceTest.js';
import update from 'immutability-helper';
import * as dh from './d100_helpers.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class DungeonNewDoor extends Component {

    constructor(props) {
        super(props);
        this.onNewDoorTestCompleted = this.onNewDoorTestCompleted.bind(this);
        this.onNewDoorTestStarted = this.onNewDoorTestStarted.bind(this);
        this.testid = '';
        if (typeof this.props.doorid !== "undefined")
            this.testid = this.props.doorid;
    }

    onNewDoorTestStarted() {
        const test = {
            id:'new_door'+this.testid,
            state:'init',
            dices:[{dice_max:100}]
        };
        return test;
    }

    onNewDoorTestCompleted(test) {
        // get a door
        var d = dh.get_door(test);
        // update according to place/direction
        const doors = this.props.area.doors;
        d = dh.update_door(doors, this.props.doorid, d);
        const index = d.index;
        var aa;
        if (index === -1)
            aa = update(this.props.area, {doors: {$push:[d]}});
        else
            aa = update(this.props.area, {doors: {[index]: {$set:d}}});
        this.props.set('area', aa);
        // status
        const s = 'You find door n°'+d.dice_value+' '+d.type;
        const l = update(this.props.log, {$push:[s]});
        this.props.set('log', l);
    }

    render() {
        var t = '';
        if (this.props.children) t = this.props.children;
        else t = 'Roll a new door';
        return (
            <span>
              <DiceTestHelp id={'new_door'+this.testid}>
                Roll D100 to get a door from Table D
                {' '}
              </DiceTestHelp>
              {' '}
              <DiceTest
                id={'new_door'+this.testid}
                onRollingStarted={this.onNewDoorTestStarted}
                onRollingCompleted={this.onNewDoorTestCompleted}
              > {' '}
                {t}</DiceTest>
            </span>
        );

    }

}

export default connect(mapStateToProps, mapDispatchToProps)(DungeonNewDoor);

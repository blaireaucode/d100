/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import DiceTest from './DiceTest.js';
import DiceTestHelp from './DiceTestHelp.js';
import * as ch from './combat_helpers.js';
import update from 'immutability-helper';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class CombatActionEncDmg extends Component {

    constructor(props) {
        super(props);
        this.onDmgCompleted = this.onDmgCompleted.bind(this);
        this.onDmgStarted = this.onDmgStarted.bind(this);
    }
    
    onDmgStarted() {
        var test = ch.new_test('test_enc_dmg', 6);
        test.dices.push({dice_max:10});
        return test;
    }
    
    onDmgCompleted(test) {
        // console.log('test', test);
        
        const d10 = test.dices[1].value;
        const d6 = test.dices[0].value;
        const hl = ch.get_hit_location(d10);
        var hit_dmg_mod = hl.DmgMod;
        if (hit_dmg_mod === 'Check') {
            hit_dmg_mod = 0;
            console.log('TODO Belt Check !!!!!!!', hit_dmg_mod);
        }
        else hit_dmg_mod = parseInt(hit_dmg_mod, 10);

        // dmg
        const encounter = this.props.encounter;
        const adv = this.props.adventurer;        
        const enc_dgm_mod = encounter.dmg;
        const armour = parseInt(adv.armour, 10);
        const def = parseInt(adv.def, 10);
        var dmg = hit_dmg_mod + d6 + enc_dgm_mod - (armour-def);
        
        if (dmg < 0) dmg = 0;
        const hp =  parseInt(adv.hp - dmg);
        var aa = update(adv, {hp: {$set:hp}});
        this.props.set('adventurer', aa);
        // console.log('ee', ee);

        
        // FIXME  Damage Deflection

        // FIXME  Defense bonus

        // status
        const s1 = 'Roll hit location = '+hl.Location+
              ' (HitDmgMod is '+hit_dmg_mod+')';
        const s2 = 'Total damage is D6 + HitDmgMod + dmg - monster def = '+d6+
              ' + '+hit_dmg_mod+' + '+enc_dgm_mod+
              ' - '+def+' = '+dmg;
        const s3 = 'TODO Damage Deflection and Defense Bonus';

        const l = update(this.props.log, {$push:[s1,s2,s3]});
        this.props.set('log', l);
    }
    

    render() {
        return (
            <span>
              <DiceTestHelp id={'test_enc_dmg'}>
                
                Roll D10 to get the dmg modifier (DmgMod) from the Hit
                Location Table.<br/>
                
                Then roll D6 to get the Dmg. <br/>
                
                To obtain the total Dmg, compute D6 + DmgMod + Dmg -
                Armour
                
              </DiceTestHelp> {' '}
              <DiceTest
                id='test_enc_dmg'
                onRollingStarted={this.onDmgStarted}
                onRollingCompleted={this.onDmgCompleted}>
                Roll damage from monster
              </DiceTest>
            </span>
        );
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(CombatActionEncDmg);


/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import DicePanel from './DicePanel.js';
import DiceTest from './DiceTest.js';
import * as ch from './combat_helpers.js';
import Grid from '@material-ui/core/Grid';

class LayoutCenter extends Component {

    onD100() {
        return ch.new_test('d100', 100);
    }
    
    onD10() {
        return ch.new_test('d10', 10);
    }
    
    onD6() {
        return ch.new_test('d6', 6);
    }
    
    render() {
        return (
            <div className={'layout'}>              
              {this.props.children}
            </div>
        );
    }
    render_old() {
        return (
            <div className={'layout'}>
              <Grid
                container
                direction="row"
                spacing={2}>
                <Grid item xs={3}>
                  <DiceTest
                    id='d100'
                    onRollingStarted={this.onD100}>
                    D100
                  </DiceTest>
                  {' - '}
                  <DiceTest
                    id='d10'
                    onRollingStarted={this.onD10}>
                    D10
                  </DiceTest>
                  {' - '}
                  <DiceTest
                    id='d6'
                    onRollingStarted={this.onD6}>
                    D6
                  </DiceTest>
                  <DicePanel/>
                </Grid>
                <Grid item xs={9}>
                  {this.props.children}
                </Grid>
              </Grid>
            </div>
        );
    }
}

export default LayoutCenter;

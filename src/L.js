/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';

class L extends Component {
    render() {
        return (
            <button className={this.props.className} onClick={this.props.onClick}>
              {this.props.children}
            </button>
        );
    }
}

export default L;


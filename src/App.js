/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import Theme from './theme.json';
import Routes from './Routes.js';
import store from './store.js';
import './App.css';

const theme = createMuiTheme(Theme);

class App extends Component {    
    render() {
        return (
            <Provider store={store}>
              <MuiThemeProvider theme={theme}>
                <div className="App">
                  <Routes/>
                </div>
              </MuiThemeProvider>
            </Provider>
        );
    }
}

export default App;

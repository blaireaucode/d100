/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import DiceTest from './DiceTest.js';
import * as ch from './combat_helpers.js';
import update from 'immutability-helper';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class CombatActionNewEncounter extends Component {

    constructor(props) {
        super(props);
        this.onNewEncTestCompleted = this.onNewEncTestCompleted.bind(this);
        this.onNewEncTestStarted = this.onNewEncTestStarted.bind(this);
    }
    
    onNewEncTestStarted() {
        // FIXME --> modify according to quest etc ...
        return ch.new_test('test_new_encounter', 100);
    }
    
    onNewEncTestCompleted(test) {
        const e = ch.get_encounter(test, this.props.quest);
        this.props.set('encounter', e);
        const v = parseInt(test.dices[0].value,10);
        const m = parseInt(this.props.quest.enc_mod || 0);
        var log = 'New encounter '+v+' (roll) + '+m+' (quest mod) = '+(v+m);
        log += ' : '+e.name;
        const l = update(this.props.log, {$push:[log]});
        this.props.set('log', l);        
    }
    
    render() {
        return (
            <DiceTest
              id='test_new_encounter'
              onRollingStarted={this.onNewEncTestStarted}
              onRollingCompleted={this.onNewEncTestCompleted}>
              New encounter
            </DiceTest>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CombatActionNewEncounter);

